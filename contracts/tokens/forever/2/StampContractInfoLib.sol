pragma ton-solidity >=0.47.0; 

    library StampContractInfo {
        function STAMP_ROOT() internal inline returns(address){
            return address(0x5f011205e69f68ec807164af0d8643037c872e965489720f6d5a14885d0e8534);
        }
        uint256 constant STAMP_ROOT_PUBKEY = 0xf93ba311ca9a2bac0c4a4a2bf3f853abb96618d6ac5645862daab4a2affa0011;
        uint256 constant STAMP_CODEHASH = 0x4a6ea7eadf31804146f8d8d83b7434755c3532fa4b78f629111b852a41c874d6;
        uint16 constant  STAMP_CODEDEPTH = 9;
    }