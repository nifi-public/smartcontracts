const ForeverTokenContract = {
    abi: {
        "ABI version": 2,
        "version": "2.2",
        "header": [
            "pubkey",
            "time",
            "expire"
        ],
        "functions": [
            {
                "name": "constructor",
                "inputs": [
                    {
                        "name": "owner",
                        "type": "address"
                    },
                    {
                        "name": "manager",
                        "type": "address"
                    },
                    {
                        "name": "managerUnlockTime",
                        "type": "uint32"
                    },
                    {
                        "name": "creator",
                        "type": "address"
                    },
                    {
                        "name": "creatorPercent",
                        "type": "uint32"
                    },
                    {
                        "name": "creationTopup",
                        "type": "uint128"
                    },
                    {
                        "name": "hash",
                        "type": "uint256"
                    },
                    {
                        "name": "delForeverCost",
                        "type": "uint128"
                    }
                ],
                "outputs": []
            },
            {
                "name": "changeOwner",
                "inputs": [
                    {
                        "name": "owner",
                        "type": "address"
                    }
                ],
                "outputs": []
            },
            {
                "name": "receiveArtInfo",
                "inputs": [
                    {
                        "name": "answerId",
                        "type": "uint32"
                    }
                ],
                "outputs": [
                    {
                        "name": "creator",
                        "type": "address"
                    },
                    {
                        "name": "creatorPercent",
                        "type": "uint32"
                    },
                    {
                        "name": "hash",
                        "type": "uint256"
                    }
                ]
            },
            {
                "name": "getArtInfo",
                "inputs": [],
                "outputs": [
                    {
                        "name": "creator",
                        "type": "address"
                    },
                    {
                        "name": "creatorPercent",
                        "type": "uint32"
                    },
                    {
                        "name": "hash",
                        "type": "uint256"
                    }
                ]
            },
            {
                "name": "getInfo",
                "inputs": [],
                "outputs": [
                    {
                        "name": "root",
                        "type": "address"
                    },
                    {
                        "name": "id",
                        "type": "uint64"
                    }
                ]
            },
            {
                "name": "receiveTradeInfo",
                "inputs": [
                    {
                        "name": "answerId",
                        "type": "uint32"
                    }
                ],
                "outputs": [
                    {
                        "name": "owner",
                        "type": "address"
                    },
                    {
                        "name": "creator",
                        "type": "address"
                    },
                    {
                        "name": "creatorPercent",
                        "type": "uint32"
                    },
                    {
                        "name": "manager",
                        "type": "address"
                    },
                    {
                        "name": "managerUnlockTime",
                        "type": "uint32"
                    }
                ]
            },
            {
                "name": "getTradeInfo",
                "inputs": [],
                "outputs": [
                    {
                        "name": "owner",
                        "type": "address"
                    },
                    {
                        "name": "creator",
                        "type": "address"
                    },
                    {
                        "name": "creatorPercent",
                        "type": "uint32"
                    },
                    {
                        "name": "manager",
                        "type": "address"
                    },
                    {
                        "name": "managerUnlockTime",
                        "type": "uint32"
                    }
                ]
            },
            {
                "name": "getStamps",
                "inputs": [],
                "outputs": [
                    {
                        "components": [
                            {
                                "name": "stamp",
                                "type": "address"
                            },
                            {
                                "name": "owner",
                                "type": "address"
                            },
                            {
                                "name": "seal",
                                "type": "address"
                            },
                            {
                                "name": "place",
                                "type": "uint8"
                            }
                        ],
                        "name": "stamps",
                        "type": "tuple[]"
                    }
                ]
            },
            {
                "name": "lockManager",
                "inputs": [
                    {
                        "name": "manager",
                        "type": "address"
                    },
                    {
                        "name": "unlockTime",
                        "type": "uint32"
                    }
                ],
                "outputs": []
            },
            {
                "name": "unlock",
                "inputs": [],
                "outputs": []
            },
            {
                "name": "addStamp",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "owner",
                        "type": "address"
                    },
                    {
                        "name": "seal",
                        "type": "address"
                    },
                    {
                        "name": "place",
                        "type": "uint8"
                    }
                ],
                "outputs": []
            },
            {
                "name": "getParameters",
                "inputs": [],
                "outputs": [
                    {
                        "name": "delForeverCost",
                        "type": "uint128"
                    }
                ]
            }
        ],
        "data": [
            {
                "key": 1,
                "name": "_root",
                "type": "address"
            },
            {
                "key": 2,
                "name": "_id",
                "type": "uint64"
            }
        ],
        "events": [
            {
                "name": "TK_CO_nifi_for1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "newOwner",
                        "type": "address"
                    }
                ],
                "outputs": []
            },
            {
                "name": "TK_MG_nifi_for1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "newManager",
                        "type": "address"
                    },
                    {
                        "name": "expirationTime",
                        "type": "uint32"
                    }
                ],
                "outputs": []
            },
            {
                "name": "FOR_SC_nifi_for1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "token1Address",
                        "type": "address"
                    },
                    {
                        "name": "token2Address",
                        "type": "address"
                    },
                    {
                        "name": "token3Address",
                        "type": "address"
                    },
                    {
                        "name": "token4Address",
                        "type": "address"
                    },
                    {
                        "name": "sealAddress",
                        "type": "address"
                    }
                ],
                "outputs": []
            },
            {
                "name": "FOR_EX_nifi_for1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    }
                ],
                "outputs": []
            }
        ],
        "fields": [
            {
                "name": "_pubkey",
                "type": "uint256"
            },
            {
                "name": "_timestamp",
                "type": "uint64"
            },
            {
                "name": "_constructorFlag",
                "type": "bool"
            },
            {
                "name": "_root",
                "type": "address"
            },
            {
                "name": "_id",
                "type": "uint64"
            },
            {
                "name": "_owner",
                "type": "address"
            },
            {
                "name": "_manager",
                "type": "address"
            },
            {
                "name": "_managerUnlockTime",
                "type": "uint32"
            },
            {
                "name": "_creator",
                "type": "address"
            },
            {
                "name": "_creatorPercent",
                "type": "uint32"
            },
            {
                "name": "_hash",
                "type": "uint256"
            },
            {
                "components": [
                    {
                        "name": "stamp",
                        "type": "address"
                    },
                    {
                        "name": "owner",
                        "type": "address"
                    },
                    {
                        "name": "seal",
                        "type": "address"
                    },
                    {
                        "name": "place",
                        "type": "uint8"
                    }
                ],
                "name": "_stamps",
                "type": "tuple[]"
            },
            {
                "name": "_delForeverCost",
                "type": "uint128"
            },
            {
                "name": "_creationTopup",
                "type": "uint128"
            }
        ]
    },
    tvc: "te6ccgECQgEAC2wAAgE0AwEBAcACAEPQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgBCSK7VMg4wMgwP/jAiDA/uMC8gs/BQRBA7ztRNDXScMB+GaJ+Gkh2zzTAAGOGYMI1xgg+QEB0wABlNP/AwGTAvhC4vkQ8qiV0wAB8nri0z8B+EMhufK0IPgjgQPoqIIIG3dAoLnytPhj0x8B+CO88rnTHwHbPPI8NjMGA3rtRNDXScMB+GYi0NMD+kAw+GmpOAD4RH9vcYIImJaAb3Jtb3Nwb3T4ZNwhxwDjAiHXDR/yvCHjAwHbPPI8Pj4GAzwgghAhezMIu+MCIIIQRWQ8aLvjAiCCEHOYR+a74wIoEQcEUCCCEFzE5ra64wIgghBsH0+wuuMCIIIQb9pz/rrjAiCCEHOYR+a64wIODQoIA2gw+Eby4Ez4Qm7jANHbPCGOHCPQ0wH6QDAxyM+HIM6CEPOYR+bPC4HLf8lw+wCRMOLjAPIAPQk7AAT4UwOKMPhG8uBM+EJu4wDTH/hEWG91+GTR2zwljisn0NMB+kAwMcjPhyDOcc8LYV5AyM+Tv2nP+s5VMMjOyx9ZyM7LH83NzclwPQwLAZCOP/hEIG8TIW8S+ElVAm8RyM+EgMoAz4RAzgH6AvQAcc8LaV5AyPhEbxXPCx/OVTDIzssfWcjOyx/Nzc3J+ERvFOL7AOMA8gA7ASD4RHBvcnBvcYBAb3T4ZNs8PAN6MPhG8uBM+EJu4wDR2zwjjiQl0NMB+kAwMcjPhyDOcc8LYV4gyM+TsH0+ws7LH8v/zclw+wCSXwPi4wDyAD05OwMmMPhG8uBM+EJu4wDR2zww2zzyAD0PLwEqiPhJ+E3HBfgj+E65sPLoZ/gAcPhuEAA8TWV0aG9kIGZvciBsb2NrZWQgbWFuYWdlciBvbmx5BFAgghAi4gIvuuMCIIIQLM0kn7rjAiCCEEPbWrK64wIgghBFZDxouuMCGRcVEgM6MPhG8uBM+EJu4wAhk9TR0N76QNMf0ds8MNs88gA9Ey8Euoj4SfhMxwX4I/hOvrD4SfhNxwX4I/hOubCx8uhoiCL6Qm8T1wv/wwDy6GqI+CMiufLoafgAAfhtIPhu+E34S9s8yM+HIM5xzwthVSDIz5FkcIQGyz/Oyx/NyXD7ACwwFCsAIkludmFsaWQgbG9jayB0aW1lA3Qw+Eby4Ez4Qm7jANHbPCGOIiPQ0wH6QDAxyM+HIM6CEMPbWrLPC4EBbyICyx/0AMlw+wCRMOLjAPIAPRY7AAT4UgNyMPhG8uBM+EJu4wDR2zwijiEk0NMB+kAwMcjPhyDOcc8LYQLIz5KzNJJ+zss/zclw+wCRW+LjAPIAPRg7AAj4SvhLA04w+Eby4Ez4Qm7jACGV0z/U0dCS0z/i+kDU0dD6QNMH0ds8MNs88gA9Gi8D/PhMI8cF8uBrgvBchBAeXMhlDohMJO1CIQW7EV5k0q/77WI2pPimqccVNcjL/3BtgED0Q9s8cViAQPQWJMjLP3JYgED0Q8j0AMkg+QAh12WC8K+ISOnuE1qPIkaRAMRQUwzdcgH2YtQ2jeaVQHH5mHaoInkj2zz4SSHIz4oAQCcmGwFWy//J0McF8uBq+FJvEMEEjoCOFfhJyM+FiM6CEF+Th9XPC47JgED7AOJfCBwCRPhS+ElfOG8E2zzJAW8iIaRVIIAg9BdvAiD4cm8QwASOgN4lHQRMcPhSbxGAIPQP8rLQ2zxvE3BxkyDBBI6A4xgwAcMPsY6AjoDi+wAkIiAeBHhw+FJvEYAg9A/ystDbPG8Sc/hSbxGAIPQP8rLQ2zxvEHL4Um8RgCD0D/Ky0Ns8bxBx+FJvEYAg9A/ystAkJCQfA67bPG8QcPhSbxGAIPQP8rLQ2zxvEPhL2zzIz4cgznHPC2FVUMjPkAGdazbLP85VMMjOVSDIzlnIzgHIzs3Nzc3NyXD7APhUcPsC+ErIz4WIzoBvz0DJgwYkJCsE/PhTcPhSbxGAIPQP8rLQ2zxvEMjPhYjOAfoCghBfk4fVzwuKyXD7APhTcfhSbxGAIPQP8rLQ2zxvEMjPhYjOAfoCghBfk4fVzwuKyXD7APhTcvhSbxGAIPQP8rLQ2zxvEMjPhYjOAfoCghBfk4fVzwuKyXD7APhTc/hSbxGAICQkJCECiPQP8rLQ2zxvEMjPhYjOAfoCghBfk4fVzwuKyXD7APhL2zzIz4cgzoIKTECHzwuByz/JcPsA+ErIz4UIzoBvz0DJgQCgJCsEglMg+FJvEYAg9A/ystDbPG8TsTMg+FJvEYAg9A/ystDbPG8RcPhSbxGAIPQP8rLQ2zxvEccFsyCOgN+UfzLbMeCkJCQkIwJIMCD4Um8RgCD0D/Ky0Ns8bxJw+FJvEYAg9A/ystDbPG8SxwWzJCQAIvpA1NHQ+kDU0dD6QNMH0W8EACJvJF4gyM5VIMjOWcjOywfNzQAsyM+MCATSWM8LD8sPWM8L/8v/ydD5AgBIjQhgA+o663w5JulqbczoehClQsa/p62cFB7cF61o76cfPH1cBFAgghAUewjIuuMCIIIQFaW1lLrjAiCCEBiFoSW64wIgghAhezMIuuMCOjctKQM2MPhG8uBM+EJu4wAhk9TR0N76QNHbPDDbPPIAPSovA5qI+En4TMcF+CP4Tr6w+En4TccF+CP4TrmwsfLoaIgh+kJvE9cL/8MA8uhq+AAg+Gz4S9s8yM+HIM5xzwthWcjPkfcV/QLLP87NyXD7ACwwKwBIjQhYAIjauhi5sHE50bjRASMZ4Ep5srgIiSLgeblpuOIZCokUAEhNZXRob2QgZm9yIHRoZSBvd25lciBvciBtYW5hZ2VyIG9ubHkEejD4Qm7jAPhG8nMhk9TR0N76QNTR0PpA0x/U0dD6QNMf03/T/9TR0NN/0Yj4SfhKxwXy6GWIJIEJYbny6GszMjEuA3iIJfpCbxPXC//DAPLoaogo+kJvE9cL/8MA8uhq+ABVA/hvVQL4cAH4cVUD+GxVAvhtWPhu+HP4dNs88gAwMC8AlvhU+FP4UvhR+FD4T/hO+E34TPhL+Er4Q/hCyMv/yz/Pg87LP1WAyM5VcMjOyx9VUMjOyx/L/wFvIgLLH/QAWcjLf8t/zc3NzcntVAAqQWRkcmVzcyBjYW4ndCBiZSBudWxsAChVbnZhbGlkIGNyZWF0b3IgZmVlcwAwTWV0aG9kIGZvciB0aGUgcm9vdCBvbmx5AhbtRNDXScIBjoDjDTQ9A5hw7UTQ9AVxIYBA9A6OgN9yIoBA9A6T1ws/kXDiiSBwiXBfIG1vAnAg+HT4c/hy+HH4cPhv+G74bfhs+Gv4aoBA9A7yvdcL//hicPhjNTY2AQKJNgBDgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAP2MPhG8uBM+EJu4wDTH/hEWG91+GTR2zwjjiIl0NMB+kAwMcjPhyDOcc8LYV4gyM+SVpbWUs7LH8v/zclwjjb4RCBvEyFvEvhJVQJvEcjPhIDKAM+EQM4B+gL0AHHPC2leIMj4RG8Vzwsfzssfy//NyfhEbxTi+wDjAPIAPTg7ASD4RHBvcnBvcYBAb3T4ZNs8OQAM+E/4UPhRA4ww+Eby4Ez4Qm7jANHbPCWOLSfQ0wH6QDAxyM+HIM5xzwthXkDIz5JR7CMizlUwyM7LH1nIzssfzc3NyXD7AJJfBeLjAPIAPTw7ACjtRNDT/9M/MfhDWMjL/8s/zsntVAAU+Ez4T/hQ+E34TgCa7UTQ0//TP9MAMfpA0z/U0dD6QNTR0PpA0x/U0dD6QNMf0//TH/QEWW8CAdTR0NN/03/R+HT4c/hy+HH4cPhv+G74bfhs+Gv4avhj+GIACvhG8uBMAgr0pCD0oUFAABRzb2wgMC42MS4wAAA=",
    code: "te6ccgECPwEACz8ABCSK7VMg4wMgwP/jAiDA/uMC8gs8AgE+A7ztRNDXScMB+GaJ+Gkh2zzTAAGOGYMI1xgg+QEB0wABlNP/AwGTAvhC4vkQ8qiV0wAB8nri0z8B+EMhufK0IPgjgQPoqIIIG3dAoLnytPhj0x8B+CO88rnTHwHbPPI8MzADA3rtRNDXScMB+GYi0NMD+kAw+GmpOAD4RH9vcYIImJaAb3Jtb3Nwb3T4ZNwhxwDjAiHXDR/yvCHjAwHbPPI8OzsDAzwgghAhezMIu+MCIIIQRWQ8aLvjAiCCEHOYR+a74wIlDgQEUCCCEFzE5ra64wIgghBsH0+wuuMCIIIQb9pz/rrjAiCCEHOYR+a64wILCgcFA2gw+Eby4Ez4Qm7jANHbPCGOHCPQ0wH6QDAxyM+HIM6CEPOYR+bPC4HLf8lw+wCRMOLjAPIAOgY4AAT4UwOKMPhG8uBM+EJu4wDTH/hEWG91+GTR2zwljisn0NMB+kAwMcjPhyDOcc8LYV5AyM+Tv2nP+s5VMMjOyx9ZyM7LH83NzclwOgkIAZCOP/hEIG8TIW8S+ElVAm8RyM+EgMoAz4RAzgH6AvQAcc8LaV5AyPhEbxXPCx/OVTDIzssfWcjOyx/Nzc3J+ERvFOL7AOMA8gA4ASD4RHBvcnBvcYBAb3T4ZNs8OQN6MPhG8uBM+EJu4wDR2zwjjiQl0NMB+kAwMcjPhyDOcc8LYV4gyM+TsH0+ws7LH8v/zclw+wCSXwPi4wDyADo2OAMmMPhG8uBM+EJu4wDR2zww2zzyADoMLAEqiPhJ+E3HBfgj+E65sPLoZ/gAcPhuDQA8TWV0aG9kIGZvciBsb2NrZWQgbWFuYWdlciBvbmx5BFAgghAi4gIvuuMCIIIQLM0kn7rjAiCCEEPbWrK64wIgghBFZDxouuMCFhQSDwM6MPhG8uBM+EJu4wAhk9TR0N76QNMf0ds8MNs88gA6ECwEuoj4SfhMxwX4I/hOvrD4SfhNxwX4I/hOubCx8uhoiCL6Qm8T1wv/wwDy6GqI+CMiufLoafgAAfhtIPhu+E34S9s8yM+HIM5xzwthVSDIz5FkcIQGyz/Oyx/NyXD7ACktESgAIkludmFsaWQgbG9jayB0aW1lA3Qw+Eby4Ez4Qm7jANHbPCGOIiPQ0wH6QDAxyM+HIM6CEMPbWrLPC4EBbyICyx/0AMlw+wCRMOLjAPIAOhM4AAT4UgNyMPhG8uBM+EJu4wDR2zwijiEk0NMB+kAwMcjPhyDOcc8LYQLIz5KzNJJ+zss/zclw+wCRW+LjAPIAOhU4AAj4SvhLA04w+Eby4Ez4Qm7jACGV0z/U0dCS0z/i+kDU0dD6QNMH0ds8MNs88gA6FywD/PhMI8cF8uBrgvBchBAeXMhlDohMJO1CIQW7EV5k0q/77WI2pPimqccVNcjL/3BtgED0Q9s8cViAQPQWJMjLP3JYgED0Q8j0AMkg+QAh12WC8K+ISOnuE1qPIkaRAMRQUwzdcgH2YtQ2jeaVQHH5mHaoInkj2zz4SSHIz4oAQCQjGAFWy//J0McF8uBq+FJvEMEEjoCOFfhJyM+FiM6CEF+Th9XPC47JgED7AOJfCBkCRPhS+ElfOG8E2zzJAW8iIaRVIIAg9BdvAiD4cm8QwASOgN4iGgRMcPhSbxGAIPQP8rLQ2zxvE3BxkyDBBI6A4xgwAcMPsY6AjoDi+wAhHx0bBHhw+FJvEYAg9A/ystDbPG8Sc/hSbxGAIPQP8rLQ2zxvEHL4Um8RgCD0D/Ky0Ns8bxBx+FJvEYAg9A/ystAhISEcA67bPG8QcPhSbxGAIPQP8rLQ2zxvEPhL2zzIz4cgznHPC2FVUMjPkAGdazbLP85VMMjOVSDIzlnIzgHIzs3Nzc3NyXD7APhUcPsC+ErIz4WIzoBvz0DJgwYhISgE/PhTcPhSbxGAIPQP8rLQ2zxvEMjPhYjOAfoCghBfk4fVzwuKyXD7APhTcfhSbxGAIPQP8rLQ2zxvEMjPhYjOAfoCghBfk4fVzwuKyXD7APhTcvhSbxGAIPQP8rLQ2zxvEMjPhYjOAfoCghBfk4fVzwuKyXD7APhTc/hSbxGAICEhIR4CiPQP8rLQ2zxvEMjPhYjOAfoCghBfk4fVzwuKyXD7APhL2zzIz4cgzoIKTECHzwuByz/JcPsA+ErIz4UIzoBvz0DJgQCgISgEglMg+FJvEYAg9A/ystDbPG8TsTMg+FJvEYAg9A/ystDbPG8RcPhSbxGAIPQP8rLQ2zxvEccFsyCOgN+UfzLbMeCkISEhIAJIMCD4Um8RgCD0D/Ky0Ns8bxJw+FJvEYAg9A/ystDbPG8SxwWzISEAIvpA1NHQ+kDU0dD6QNMH0W8EACJvJF4gyM5VIMjOWcjOywfNzQAsyM+MCATSWM8LD8sPWM8L/8v/ydD5AgBIjQhgA+o663w5JulqbczoehClQsa/p62cFB7cF61o76cfPH1cBFAgghAUewjIuuMCIIIQFaW1lLrjAiCCEBiFoSW64wIgghAhezMIuuMCNzQqJgM2MPhG8uBM+EJu4wAhk9TR0N76QNHbPDDbPPIAOicsA5qI+En4TMcF+CP4Tr6w+En4TccF+CP4TrmwsfLoaIgh+kJvE9cL/8MA8uhq+AAg+Gz4S9s8yM+HIM5xzwthWcjPkfcV/QLLP87NyXD7ACktKABIjQhYAIjauhi5sHE50bjRASMZ4Ep5srgIiSLgeblpuOIZCokUAEhNZXRob2QgZm9yIHRoZSBvd25lciBvciBtYW5hZ2VyIG9ubHkEejD4Qm7jAPhG8nMhk9TR0N76QNTR0PpA0x/U0dD6QNMf03/T/9TR0NN/0Yj4SfhKxwXy6GWIJIEJYbny6GswLy4rA3iIJfpCbxPXC//DAPLoaogo+kJvE9cL/8MA8uhq+ABVA/hvVQL4cAH4cVUD+GxVAvhtWPhu+HP4dNs88gAtLSwAlvhU+FP4UvhR+FD4T/hO+E34TPhL+Er4Q/hCyMv/yz/Pg87LP1WAyM5VcMjOyx9VUMjOyx/L/wFvIgLLH/QAWcjLf8t/zc3NzcntVAAqQWRkcmVzcyBjYW4ndCBiZSBudWxsAChVbnZhbGlkIGNyZWF0b3IgZmVlcwAwTWV0aG9kIGZvciB0aGUgcm9vdCBvbmx5AhbtRNDXScIBjoDjDTE6A5hw7UTQ9AVxIYBA9A6OgN9yIoBA9A6T1ws/kXDiiSBwiXBfIG1vAnAg+HT4c/hy+HH4cPhv+G74bfhs+Gv4aoBA9A7yvdcL//hicPhjMjMzAQKJMwBDgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAP2MPhG8uBM+EJu4wDTH/hEWG91+GTR2zwjjiIl0NMB+kAwMcjPhyDOcc8LYV4gyM+SVpbWUs7LH8v/zclwjjb4RCBvEyFvEvhJVQJvEcjPhIDKAM+EQM4B+gL0AHHPC2leIMj4RG8Vzwsfzssfy//NyfhEbxTi+wDjAPIAOjU4ASD4RHBvcnBvcYBAb3T4ZNs8NgAM+E/4UPhRA4ww+Eby4Ez4Qm7jANHbPCWOLSfQ0wH6QDAxyM+HIM5xzwthXkDIz5JR7CMizlUwyM7LH1nIzssfzc3NyXD7AJJfBeLjAPIAOjk4ACjtRNDT/9M/MfhDWMjL/8s/zsntVAAU+Ez4T/hQ+E34TgCa7UTQ0//TP9MAMfpA0z/U0dD6QNTR0PpA0x/U0dD6QNMf0//TH/QEWW8CAdTR0NN/03/R+HT4c/hy+HH4cPhv+G74bfhs+Gv4avhj+GIACvhG8uBMAgr0pCD0oT49ABRzb2wgMC42MS4wAAA=",
    codeHash: "95eff819418eff4748bbcf8d0ea3957b2a45b8c629abdebdbc432c6da9a1ef47",
};
module.exports = { ForeverTokenContract };