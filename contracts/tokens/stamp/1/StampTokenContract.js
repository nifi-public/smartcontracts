const StampTokenContract = {
    abi: {
        "ABI version": 2,
        "version": "2.2",
        "header": [
            "pubkey",
            "time",
            "expire"
        ],
        "functions": [
            {
                "name": "constructor",
                "inputs": [
                    {
                        "name": "owner",
                        "type": "address"
                    },
                    {
                        "name": "manager",
                        "type": "address"
                    },
                    {
                        "name": "managerUnlockTime",
                        "type": "uint32"
                    },
                    {
                        "name": "creator",
                        "type": "address"
                    },
                    {
                        "name": "creatorPercent",
                        "type": "uint32"
                    },
                    {
                        "name": "hash",
                        "type": "uint256"
                    },
                    {
                        "name": "minRequestEndorseFee",
                        "type": "uint128"
                    },
                    {
                        "name": "minCancelEndorseFee",
                        "type": "uint128"
                    },
                    {
                        "name": "minForAddFee",
                        "type": "uint128"
                    },
                    {
                        "name": "forAddStampCost",
                        "type": "uint128"
                    },
                    {
                        "name": "endorseIncomePercent",
                        "type": "uint16"
                    }
                ],
                "outputs": []
            },
            {
                "name": "changeOwner",
                "inputs": [
                    {
                        "name": "owner",
                        "type": "address"
                    }
                ],
                "outputs": []
            },
            {
                "name": "receiveArtInfo",
                "inputs": [
                    {
                        "name": "answerId",
                        "type": "uint32"
                    }
                ],
                "outputs": [
                    {
                        "name": "creator",
                        "type": "address"
                    },
                    {
                        "name": "creatorPercent",
                        "type": "uint32"
                    },
                    {
                        "name": "hash",
                        "type": "uint256"
                    }
                ]
            },
            {
                "name": "getArtInfo",
                "inputs": [],
                "outputs": [
                    {
                        "name": "creator",
                        "type": "address"
                    },
                    {
                        "name": "creatorPercent",
                        "type": "uint32"
                    },
                    {
                        "name": "hash",
                        "type": "uint256"
                    }
                ]
            },
            {
                "name": "getSeal",
                "inputs": [],
                "outputs": [
                    {
                        "name": "seal",
                        "type": "optional(address)"
                    },
                    {
                        "name": "corner",
                        "type": "uint8"
                    }
                ]
            },
            {
                "name": "getForever",
                "inputs": [],
                "outputs": [
                    {
                        "name": "forever",
                        "type": "optional(address)"
                    }
                ]
            },
            {
                "name": "getInfo",
                "inputs": [],
                "outputs": [
                    {
                        "name": "root",
                        "type": "address"
                    },
                    {
                        "name": "id",
                        "type": "uint64"
                    }
                ]
            },
            {
                "name": "receiveTradeInfo",
                "inputs": [
                    {
                        "name": "answerId",
                        "type": "uint32"
                    }
                ],
                "outputs": [
                    {
                        "name": "owner",
                        "type": "address"
                    },
                    {
                        "name": "creator",
                        "type": "address"
                    },
                    {
                        "name": "creatorPercent",
                        "type": "uint32"
                    },
                    {
                        "name": "manager",
                        "type": "address"
                    },
                    {
                        "name": "managerUnlockTime",
                        "type": "uint32"
                    }
                ]
            },
            {
                "name": "getTradeInfo",
                "inputs": [],
                "outputs": [
                    {
                        "name": "owner",
                        "type": "address"
                    },
                    {
                        "name": "creator",
                        "type": "address"
                    },
                    {
                        "name": "creatorPercent",
                        "type": "uint32"
                    },
                    {
                        "name": "manager",
                        "type": "address"
                    },
                    {
                        "name": "managerUnlockTime",
                        "type": "uint32"
                    }
                ]
            },
            {
                "name": "lockManager",
                "inputs": [
                    {
                        "name": "manager",
                        "type": "address"
                    },
                    {
                        "name": "unlockTime",
                        "type": "uint32"
                    }
                ],
                "outputs": []
            },
            {
                "name": "unlock",
                "inputs": [],
                "outputs": []
            },
            {
                "name": "requestEndorse",
                "inputs": [
                    {
                        "name": "seal",
                        "type": "address"
                    },
                    {
                        "name": "places",
                        "type": "uint8"
                    },
                    {
                        "name": "price",
                        "type": "uint128"
                    }
                ],
                "outputs": []
            },
            {
                "name": "cancelEndorse",
                "inputs": [],
                "outputs": []
            },
            {
                "name": "endorse",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "place",
                        "type": "uint8"
                    },
                    {
                        "name": "sealOwner",
                        "type": "address"
                    }
                ],
                "outputs": []
            },
            {
                "name": "setForever",
                "inputs": [
                    {
                        "name": "forever",
                        "type": "address"
                    }
                ],
                "outputs": []
            },
            {
                "name": "delForever",
                "inputs": [],
                "outputs": []
            },
            {
                "name": "getParameters",
                "inputs": [],
                "outputs": [
                    {
                        "name": "minRequestEndorseFee",
                        "type": "uint128"
                    },
                    {
                        "name": "minCancelEndorseFee",
                        "type": "uint128"
                    },
                    {
                        "name": "minForAddFee",
                        "type": "uint128"
                    },
                    {
                        "name": "forAddStampCost",
                        "type": "uint128"
                    }
                ]
            }
        ],
        "data": [
            {
                "key": 1,
                "name": "_root",
                "type": "address"
            },
            {
                "key": 2,
                "name": "_id",
                "type": "uint64"
            }
        ],
        "events": [
            {
                "name": "TK_CO_nifi_stamp1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "newOwner",
                        "type": "address"
                    }
                ],
                "outputs": []
            },
            {
                "name": "TK_MG_nifi_stamp1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "newManager",
                        "type": "address"
                    },
                    {
                        "name": "expirationTime",
                        "type": "uint32"
                    }
                ],
                "outputs": []
            },
            {
                "name": "TK_RQ_nifi_stamp1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "seal",
                        "type": "address"
                    },
                    {
                        "name": "sealPlaces",
                        "type": "uint8"
                    },
                    {
                        "name": "value",
                        "type": "uint64"
                    }
                ],
                "outputs": []
            },
            {
                "name": "TK_RX_nifi_stamp1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    }
                ],
                "outputs": []
            },
            {
                "name": "TK_EN_nifi_stamp1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "seal",
                        "type": "address"
                    },
                    {
                        "name": "corner",
                        "type": "uint8"
                    }
                ],
                "outputs": []
            },
            {
                "name": "TK_FE_nifi_stamp1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "forever",
                        "type": "address"
                    }
                ],
                "outputs": []
            },
            {
                "name": "TK_FD_nifi_stamp1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "forever",
                        "type": "address"
                    }
                ],
                "outputs": []
            }
        ],
        "fields": [
            {
                "name": "_pubkey",
                "type": "uint256"
            },
            {
                "name": "_timestamp",
                "type": "uint64"
            },
            {
                "name": "_constructorFlag",
                "type": "bool"
            },
            {
                "name": "_root",
                "type": "address"
            },
            {
                "name": "_id",
                "type": "uint64"
            },
            {
                "name": "_owner",
                "type": "address"
            },
            {
                "name": "_manager",
                "type": "address"
            },
            {
                "name": "_managerUnlockTime",
                "type": "uint32"
            },
            {
                "name": "_creator",
                "type": "address"
            },
            {
                "name": "_creatorPercent",
                "type": "uint32"
            },
            {
                "name": "_hash",
                "type": "uint256"
            },
            {
                "name": "_sealPlace",
                "type": "uint8"
            },
            {
                "name": "_seal",
                "type": "optional(address)"
            },
            {
                "name": "_sealValue",
                "type": "uint128"
            },
            {
                "name": "_sealPosiblePlaces",
                "type": "uint8"
            },
            {
                "name": "_forever",
                "type": "optional(address)"
            },
            {
                "name": "_minRequestEndorseFee",
                "type": "uint128"
            },
            {
                "name": "_minCancelEndorseFee",
                "type": "uint128"
            },
            {
                "name": "_minForAddFee",
                "type": "uint128"
            },
            {
                "name": "_forAddStampCost",
                "type": "uint128"
            },
            {
                "name": "_endorseIncomePercent",
                "type": "uint16"
            }
        ]
    },
    tvc: "te6ccgECRwEADPEAAgE0AwEBAcACAEPQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgBCSK7VMg4wMgwP/jAiDA/uMC8gtEBQRGA7ztRNDXScMB+GaJ+Gkh2zzTAAGOGYMI1xgg+QEB0wABlNP/AwGTAvhC4vkQ8qiV0wAB8nri0z8B+EMhufK0IPgjgQPoqIIIG3dAoLnytPhj0x8B+CO88rnTHwHbPPI8OzgGA3rtRNDXScMB+GYi0NMD+kAw+GmpOAD4RH9vcYIImJaAb3Jtb3Nwb3T4ZNwhxwDjAiHXDR/yvCHjAwHbPPI8Q0MGAiggghBsH0+wu+MCIIIQb9pz/rrjAgoHA4ow+Eby4Ez4Qm7jANMf+ERYb3X4ZNHbPCWOKyfQ0wH6QDAxyM+HIM5xzwthXkDIz5O/ac/6zlUwyM7LH1nIzssfzc3NyXBCCQgBkI4/+EQgbxMhbxL4SVUCbxHIz4SAygDPhEDOAfoC9ABxzwtpXkDI+ERvFc8LH85VMMjOyx9ZyM7LH83Nzcn4RG8U4vsA4wDyAD0BIPhEcG9ycG9xgEBvdPhk2zwvBFAgghASYBuVu+MCIIIQLM0kn7vjAiCCEEVkPGi74wIgghBsH0+wu+MCMCQUCwRQIIIQUWM4AbrjAiCCEFzE5ra64wIgghBfk4fVuuMCIIIQbB9PsLrjAhIPDQwDejD4RvLgTPhCbuMA0ds8I44kJdDTAfpAMDHIz4cgznHPC2FeIMjPk7B9PsLOyx/L/83JcPsAkl8D4uMA8gBCLT0DJjD4RvLgTPhCbuMA0ds8MNs88gBCDkABbvhWbrMgmzD4SfhWIG7yf8cF3vLgdm34dvhJ+EvbPMjPhyDOcc8LYVnIz5B9A1aSyz/Ozclw+wApAyYw+Eby4Ez4Qm7jANHbPDDbPPIAQhBAATKI+FZu+En4TccF+CP4TrmwsPLoZ/gAcPhuEQA8TWV0aG9kIGZvciBsb2NrZWQgbWFuYWdlciBvbmx5AyYw+Eby4Ez4Qm7jANHbPDDbPPIAQhNAAtCI+FZu+En4TMcF+CP4Tr6wsPLobPhS8tBu+FNu8tBvaKb+YPhYvvLgcG34c/hU+EzIz4WIzgH6AoBrz0DJcfsA+EvbPMjPhyDOghBlT4SkzwuByz/JcPsA+ErIz4WIzoBvz0DJgED7ACMpBFAgghA4oleuuuMCIIIQPIPl4LrjAiCCED/nl6664wIgghBFZDxouuMCIRsYFQM6MPhG8uBM+EJu4wAhk9TR0N76QNMf0ds8MNs88gBCFkAEwoj4Vm74SfhMxwX4I/hOvrCw+En4TccF+CP4TrmwsfLoaIgi+kJvE9cL/8MA8uhqiPgjIrny6Gn4AAH4bSD4bvhN+EvbPMjPhyDOcc8LYVUgyM+QWAk2Ass/zssfzclw+wAqNRcpACJJbnZhbGlkIGxvY2sgdGltZQM2MPhG8uBM+EJu4wAhk9TR0N76QNHbPDDbPPIAQhlAA/6I+FZu+En4TMcF+CP4Tr6wsPLobGim/mD4Wb7y4HD4Vm7y4HP4U27y0HL4UvLgdCD4diD4S9s8yM+HIM5xzwthWcjPkVEM7XbLP87NyXD7APhS+FMgbvJ/+Ez4S/haVQTIz4WIzgH6AnHPC2pVMMjPkIuICL7LP85ZyM7LB83NIykaACjJcPsA+ErIz4WIzoBvz0DJgED7AANIMPhG8uBM+EJu4wAhl9M/0wfU0dCU0z/TB+L6QNHbPDDbPPIAQhxAAsj4U26zIJsw+En4UyBu8n/HBd7y4G8h+FWw8uBxgvBchBAeXMhlDohMJO1CIQW7EV5k0q/77WI2pPimqccVNcjL/3BtgED0Q9s8cViAQPQWVQLIyz9yWIBA9EPI9ADJIPkAAddlIB0D0oLw7Z5vsBOJMC4CGEnm9xsLCKJJrtmTLEeCe+pwSbkyqzdYeFUC2zz4SQHIz4oAQMv/ydDHBfLgavgAASD4cvhTIG7yf/hL2zzIz4cgznHPC2FVIMjPkWnZe27LP87LB83JcPsA+FvCAB8pHgCQji/4VPhbgScQqYS1f/hUorV/IcjPhYjOAfoCgGvPQMlx+wD4SsjPhYjOgG/PQMmAQI4R+FQhyM+FiM4B+gKAa89AyXHi+wAwACzIz4wIBNJYzwsPyw9Yzwv/y//J0PkCAEiNCGAGpwue5HWXDUNPW/Isfc7tSctZcpG1HrS9E6HjokNRsCQDPjD4RvLgTPhCbuMAIZPU0dDe+kDTB9N/0ds8MNs88gBCIkAC4Ij4Vm74SfhMxwX4I/hOvrCw8uhsaKb+YPhXIqC1f77y4G34UvLQbiB0+wJY+HP4dCD4dfhUtT8B+FMgbvJ/+EvbPMjPhyDOcc8LYVUwyM+Q+mqcUss/zssHyz/NyXD7APhKyM+FiM6Ab89AyYMG+wAjKQAqTWV0aG9kIGZvciBvd25lciBvbmx5BFAgghAUewjIuuMCIIIQFaW1lLrjAiCCECF7Mwi64wIgghAszSSfuuMCLisnJQNyMPhG8uBM+EJu4wDR2zwijiEk0NMB+kAwMcjPhyDOcc8LYQLIz5KzNJJ+zss/zclw+wCRW+LjAPIAQiY9AAj4SvhLAzYw+Eby4Ez4Qm7jACGT1NHQ3vpA0ds8MNs88gBCKEADooj4Vm74SfhMxwX4I/hOvrCw+En4TccF+CP4TrmwsfLoaIgh+kJvE9cL/8MA8uhq+AAg+Gz4S9s8yM+HIM5xzwthWcjPkEgPEobLP87NyXD7ACo1KQBIjQhYAIjauhi5sHE50bjRASMZ4Ep5srgIiSLgeblpuOIZCokUAEhNZXRob2QgZm9yIHRoZSBvd25lciBvciBtYW5hZ2VyIG9ubHkD9jD4RvLgTPhCbuMA0x/4RFhvdfhk0ds8I44iJdDTAfpAMDHIz4cgznHPC2FeIMjPklaW1lLOyx/L/83JcI42+EQgbxMhbxL4SVUCbxHIz4SAygDPhEDOAfoC9ABxzwtpXiDI+ERvFc8LH87LH8v/zcn4RG8U4vsA4wDyAEIsPQEg+ERwb3Jwb3GAQG90+GTbPC0ADPhP+FD4UQOMMPhG8uBM+EJu4wDR2zwlji0n0NMB+kAwMcjPhyDOcc8LYV5AyM+SUewjIs5VMMjOyx9ZyM7LH83Nzclw+wCSXwXi4wDyAEIvPQAU+Ez4T/hQ+E34TgRMIIIJMZ86uuMCIIILJxzbuuMCIIIQEUuuZLrjAiCCEBJgG5W64wI/PDMxA4ow+Eby4Ez4Qm7jANHbPCKOLSTQ0wH6QDAxyM+HIM5xzwthAsjPkkmAblYBIG6TMM+BlAHPg87iywfNyXD7AJFb4uMA8gBCMj0ACPhT+FIEhjD4Qm7jAPhG8nMhk9TR0N76QNTR0PpA0x/U0dD6QNMf0//Tf9TR0NN/03/Tf9MP0Yj4SfhKxwXy6GWIJ4EJYbny6Gs4NzY0A5SIKPpCbxPXC//DAPLoaogr+kJvE9cL/8MA8uhq+ABVBvhvVQX4cFUE+HFVBvhsVQX4bVUE+G5VA/h3VQL4eFj4eQH4evh72zzyADU1QAAqQWRkcmVzcyBjYW4ndCBiZSBudWxsAChVbnZhbGlkIGNyZWF0b3IgZmVlcwAwTWV0aG9kIGZvciB0aGUgcm9vdCBvbmx5AhbtRNDXScIBjoDjDTlCA6Zw7UTQ9AVw+ED4QfhC+EP4RPhF+Eb4R/hI+ElxK4BA9A6OgN9yLIBA9A6T1ws/kXDiiSBwiXBfIG1wIG1wX0CAHG+A7VeAQPQO8r3XC//4YnD4Yzo7OwECiTsAQ4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABADhjD4RvLgTPhCbuMA0ds8IY4rI9DTAfpAMDHIz4cgznHPC2EByM+SDJxzbgEgbpMwz4GUAc+DzuLNyXD7AJEw4uMA8gBCPj0AKO1E0NP/0z8x+ENYyMv/yz/Oye1UAAT4VgOCMPhG8uBM+EJu4wDR2zwkjicm0NMB+kAwMcjPhyDOcc8LYV4wyM+SBMZ86st/y3/Lf8t/zclw+wCSXwTiMNs88gBCQUAA2u1HcIAcb4eAHW+CMIAccGRfCvhD+ELIy//LP8+Dzss/VfDIzlXgyM7LH1XAyM7LH8v/ywdVgMgBIG6TMM+BlAHPg87iy3/LB1VQyAEgbpMwz4GUAc+DzuLLf8t/y39ZyMt/yw/Nzc3Nzc3J7VQAEPhX+Fj4WfhaAOjtRNDT/9M/0wAx+kDTP9TR0PpA1NHQ+kDTH9TR0PpA0x/T/9MH1NHQ0gABkvpAkm0B4tN/0wfU0dDSAAGS+kCSbQHi03/Tf9N/1NHQ03/TD9Fw+ED4QfhC+EP4RPhF+Eb4R/hI+EmAEnpjgBxvgO1X+GP4YgAK+Eby4EwCCvSkIPShRkUAFHNvbCAwLjYxLjAAAA==",
    code: "te6ccgECRAEADMQABCSK7VMg4wMgwP/jAiDA/uMC8gtBAgFDA7ztRNDXScMB+GaJ+Gkh2zzTAAGOGYMI1xgg+QEB0wABlNP/AwGTAvhC4vkQ8qiV0wAB8nri0z8B+EMhufK0IPgjgQPoqIIIG3dAoLnytPhj0x8B+CO88rnTHwHbPPI8ODUDA3rtRNDXScMB+GYi0NMD+kAw+GmpOAD4RH9vcYIImJaAb3Jtb3Nwb3T4ZNwhxwDjAiHXDR/yvCHjAwHbPPI8QEADAiggghBsH0+wu+MCIIIQb9pz/rrjAgcEA4ow+Eby4Ez4Qm7jANMf+ERYb3X4ZNHbPCWOKyfQ0wH6QDAxyM+HIM5xzwthXkDIz5O/ac/6zlUwyM7LH1nIzssfzc3NyXA/BgUBkI4/+EQgbxMhbxL4SVUCbxHIz4SAygDPhEDOAfoC9ABxzwtpXkDI+ERvFc8LH85VMMjOyx9ZyM7LH83Nzcn4RG8U4vsA4wDyADoBIPhEcG9ycG9xgEBvdPhk2zwsBFAgghASYBuVu+MCIIIQLM0kn7vjAiCCEEVkPGi74wIgghBsH0+wu+MCLSERCARQIIIQUWM4AbrjAiCCEFzE5ra64wIgghBfk4fVuuMCIIIQbB9PsLrjAg8MCgkDejD4RvLgTPhCbuMA0ds8I44kJdDTAfpAMDHIz4cgznHPC2FeIMjPk7B9PsLOyx/L/83JcPsAkl8D4uMA8gA/KjoDJjD4RvLgTPhCbuMA0ds8MNs88gA/Cz0BbvhWbrMgmzD4SfhWIG7yf8cF3vLgdm34dvhJ+EvbPMjPhyDOcc8LYVnIz5B9A1aSyz/Ozclw+wAmAyYw+Eby4Ez4Qm7jANHbPDDbPPIAPw09ATKI+FZu+En4TccF+CP4TrmwsPLoZ/gAcPhuDgA8TWV0aG9kIGZvciBsb2NrZWQgbWFuYWdlciBvbmx5AyYw+Eby4Ez4Qm7jANHbPDDbPPIAPxA9AtCI+FZu+En4TMcF+CP4Tr6wsPLobPhS8tBu+FNu8tBvaKb+YPhYvvLgcG34c/hU+EzIz4WIzgH6AoBrz0DJcfsA+EvbPMjPhyDOghBlT4SkzwuByz/JcPsA+ErIz4WIzoBvz0DJgED7ACAmBFAgghA4oleuuuMCIIIQPIPl4LrjAiCCED/nl6664wIgghBFZDxouuMCHhgVEgM6MPhG8uBM+EJu4wAhk9TR0N76QNMf0ds8MNs88gA/Ez0Ewoj4Vm74SfhMxwX4I/hOvrCw+En4TccF+CP4TrmwsfLoaIgi+kJvE9cL/8MA8uhqiPgjIrny6Gn4AAH4bSD4bvhN+EvbPMjPhyDOcc8LYVUgyM+QWAk2Ass/zssfzclw+wAnMhQmACJJbnZhbGlkIGxvY2sgdGltZQM2MPhG8uBM+EJu4wAhk9TR0N76QNHbPDDbPPIAPxY9A/6I+FZu+En4TMcF+CP4Tr6wsPLobGim/mD4Wb7y4HD4Vm7y4HP4U27y0HL4UvLgdCD4diD4S9s8yM+HIM5xzwthWcjPkVEM7XbLP87NyXD7APhS+FMgbvJ/+Ez4S/haVQTIz4WIzgH6AnHPC2pVMMjPkIuICL7LP85ZyM7LB83NICYXACjJcPsA+ErIz4WIzoBvz0DJgED7AANIMPhG8uBM+EJu4wAhl9M/0wfU0dCU0z/TB+L6QNHbPDDbPPIAPxk9Asj4U26zIJsw+En4UyBu8n/HBd7y4G8h+FWw8uBxgvBchBAeXMhlDohMJO1CIQW7EV5k0q/77WI2pPimqccVNcjL/3BtgED0Q9s8cViAQPQWVQLIyz9yWIBA9EPI9ADJIPkAAddlHRoD0oLw7Z5vsBOJMC4CGEnm9xsLCKJJrtmTLEeCe+pwSbkyqzdYeFUC2zz4SQHIz4oAQMv/ydDHBfLgavgAASD4cvhTIG7yf/hL2zzIz4cgznHPC2FVIMjPkWnZe27LP87LB83JcPsA+FvCABwmGwCQji/4VPhbgScQqYS1f/hUorV/IcjPhYjOAfoCgGvPQMlx+wD4SsjPhYjOgG/PQMmAQI4R+FQhyM+FiM4B+gKAa89AyXHi+wAwACzIz4wIBNJYzwsPyw9Yzwv/y//J0PkCAEiNCGAGpwue5HWXDUNPW/Isfc7tSctZcpG1HrS9E6HjokNRsCQDPjD4RvLgTPhCbuMAIZPU0dDe+kDTB9N/0ds8MNs88gA/Hz0C4Ij4Vm74SfhMxwX4I/hOvrCw8uhsaKb+YPhXIqC1f77y4G34UvLQbiB0+wJY+HP4dCD4dfhUtT8B+FMgbvJ/+EvbPMjPhyDOcc8LYVUwyM+Q+mqcUss/zssHyz/NyXD7APhKyM+FiM6Ab89AyYMG+wAgJgAqTWV0aG9kIGZvciBvd25lciBvbmx5BFAgghAUewjIuuMCIIIQFaW1lLrjAiCCECF7Mwi64wIgghAszSSfuuMCKygkIgNyMPhG8uBM+EJu4wDR2zwijiEk0NMB+kAwMcjPhyDOcc8LYQLIz5KzNJJ+zss/zclw+wCRW+LjAPIAPyM6AAj4SvhLAzYw+Eby4Ez4Qm7jACGT1NHQ3vpA0ds8MNs88gA/JT0Dooj4Vm74SfhMxwX4I/hOvrCw+En4TccF+CP4TrmwsfLoaIgh+kJvE9cL/8MA8uhq+AAg+Gz4S9s8yM+HIM5xzwthWcjPkEgPEobLP87NyXD7ACcyJgBIjQhYAIjauhi5sHE50bjRASMZ4Ep5srgIiSLgeblpuOIZCokUAEhNZXRob2QgZm9yIHRoZSBvd25lciBvciBtYW5hZ2VyIG9ubHkD9jD4RvLgTPhCbuMA0x/4RFhvdfhk0ds8I44iJdDTAfpAMDHIz4cgznHPC2FeIMjPklaW1lLOyx/L/83JcI42+EQgbxMhbxL4SVUCbxHIz4SAygDPhEDOAfoC9ABxzwtpXiDI+ERvFc8LH87LH8v/zcn4RG8U4vsA4wDyAD8pOgEg+ERwb3Jwb3GAQG90+GTbPCoADPhP+FD4UQOMMPhG8uBM+EJu4wDR2zwlji0n0NMB+kAwMcjPhyDOcc8LYV5AyM+SUewjIs5VMMjOyx9ZyM7LH83Nzclw+wCSXwXi4wDyAD8sOgAU+Ez4T/hQ+E34TgRMIIIJMZ86uuMCIIILJxzbuuMCIIIQEUuuZLrjAiCCEBJgG5W64wI8OTAuA4ow+Eby4Ez4Qm7jANHbPCKOLSTQ0wH6QDAxyM+HIM5xzwthAsjPkkmAblYBIG6TMM+BlAHPg87iywfNyXD7AJFb4uMA8gA/LzoACPhT+FIEhjD4Qm7jAPhG8nMhk9TR0N76QNTR0PpA0x/U0dD6QNMf0//Tf9TR0NN/03/Tf9MP0Yj4SfhKxwXy6GWIJ4EJYbny6Gs1NDMxA5SIKPpCbxPXC//DAPLoaogr+kJvE9cL/8MA8uhq+ABVBvhvVQX4cFUE+HFVBvhsVQX4bVUE+G5VA/h3VQL4eFj4eQH4evh72zzyADIyPQAqQWRkcmVzcyBjYW4ndCBiZSBudWxsAChVbnZhbGlkIGNyZWF0b3IgZmVlcwAwTWV0aG9kIGZvciB0aGUgcm9vdCBvbmx5AhbtRNDXScIBjoDjDTY/A6Zw7UTQ9AVw+ED4QfhC+EP4RPhF+Eb4R/hI+ElxK4BA9A6OgN9yLIBA9A6T1ws/kXDiiSBwiXBfIG1wIG1wX0CAHG+A7VeAQPQO8r3XC//4YnD4Yzc4OAECiTgAQ4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABADhjD4RvLgTPhCbuMA0ds8IY4rI9DTAfpAMDHIz4cgznHPC2EByM+SDJxzbgEgbpMwz4GUAc+DzuLNyXD7AJEw4uMA8gA/OzoAKO1E0NP/0z8x+ENYyMv/yz/Oye1UAAT4VgOCMPhG8uBM+EJu4wDR2zwkjicm0NMB+kAwMcjPhyDOcc8LYV4wyM+SBMZ86st/y3/Lf8t/zclw+wCSXwTiMNs88gA/Pj0A2u1HcIAcb4eAHW+CMIAccGRfCvhD+ELIy//LP8+Dzss/VfDIzlXgyM7LH1XAyM7LH8v/ywdVgMgBIG6TMM+BlAHPg87iy3/LB1VQyAEgbpMwz4GUAc+DzuLLf8t/y39ZyMt/yw/Nzc3Nzc3J7VQAEPhX+Fj4WfhaAOjtRNDT/9M/0wAx+kDTP9TR0PpA1NHQ+kDTH9TR0PpA0x/T/9MH1NHQ0gABkvpAkm0B4tN/0wfU0dDSAAGS+kCSbQHi03/Tf9N/1NHQ03/TD9Fw+ED4QfhC+EP4RPhF+Eb4R/hI+EmAEnpjgBxvgO1X+GP4YgAK+Eby4EwCCvSkIPShQ0IAFHNvbCAwLjYxLjAAAA==",
    codeHash: "59dc7eee5e35c8bc34f481e0c2ece8e9bc10e9200c7b975468af91c132a21b4c",
};
module.exports = { StampTokenContract };