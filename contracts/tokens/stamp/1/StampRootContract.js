const StampRootContract = {
    abi: {
        "ABI version": 2,
        "version": "2.2",
        "header": [
            "pubkey",
            "time",
            "expire"
        ],
        "functions": [
            {
                "name": "constructor",
                "inputs": [
                    {
                        "name": "manager",
                        "type": "address"
                    },
                    {
                        "name": "minCreationFee",
                        "type": "uint128"
                    },
                    {
                        "name": "creationTopup",
                        "type": "uint128"
                    },
                    {
                        "name": "name",
                        "type": "string"
                    },
                    {
                        "name": "symbol",
                        "type": "string"
                    },
                    {
                        "name": "tokenCode",
                        "type": "cell"
                    }
                ],
                "outputs": []
            },
            {
                "name": "getManager",
                "inputs": [],
                "outputs": [
                    {
                        "name": "value0",
                        "type": "address"
                    }
                ]
            },
            {
                "name": "changeManager",
                "inputs": [
                    {
                        "name": "newManager",
                        "type": "address"
                    }
                ],
                "outputs": []
            },
            {
                "name": "getInfo",
                "inputs": [],
                "outputs": [
                    {
                        "name": "name",
                        "type": "string"
                    },
                    {
                        "name": "symbol",
                        "type": "string"
                    },
                    {
                        "name": "totalSupply",
                        "type": "uint128"
                    }
                ]
            },
            {
                "name": "withdraw",
                "inputs": [
                    {
                        "name": "addr",
                        "type": "address"
                    },
                    {
                        "name": "value",
                        "type": "uint128"
                    },
                    {
                        "name": "bounce",
                        "type": "bool"
                    }
                ],
                "outputs": []
            },
            {
                "name": "create",
                "inputs": [
                    {
                        "name": "creatorPercent",
                        "type": "uint32"
                    },
                    {
                        "name": "hash",
                        "type": "uint256"
                    }
                ],
                "outputs": [
                    {
                        "name": "addr",
                        "type": "address"
                    }
                ]
            },
            {
                "name": "getTokenAddress",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    }
                ],
                "outputs": [
                    {
                        "name": "addr",
                        "type": "address"
                    }
                ]
            },
            {
                "name": "setCreationParameters",
                "inputs": [
                    {
                        "name": "minCreationFee",
                        "type": "uint128"
                    },
                    {
                        "name": "creationTopup",
                        "type": "uint128"
                    }
                ],
                "outputs": []
            },
            {
                "name": "getCreationParameters",
                "inputs": [],
                "outputs": [
                    {
                        "name": "minCreationFee",
                        "type": "uint128"
                    },
                    {
                        "name": "creationTopup",
                        "type": "uint128"
                    }
                ]
            },
            {
                "name": "setStampParameters",
                "inputs": [
                    {
                        "name": "minSealFee",
                        "type": "uint128"
                    },
                    {
                        "name": "minSealRxFee",
                        "type": "uint128"
                    },
                    {
                        "name": "requestEndorseFixIncome",
                        "type": "uint128"
                    },
                    {
                        "name": "minForAddFee",
                        "type": "uint128"
                    },
                    {
                        "name": "forAddStampCost",
                        "type": "uint128"
                    },
                    {
                        "name": "endorseIncomePercent",
                        "type": "uint16"
                    }
                ],
                "outputs": []
            },
            {
                "name": "getStampParameters",
                "inputs": [],
                "outputs": [
                    {
                        "name": "minSealFee",
                        "type": "uint128"
                    },
                    {
                        "name": "minSealRxFee",
                        "type": "uint128"
                    },
                    {
                        "name": "requestEndorseFixIncome",
                        "type": "uint128"
                    },
                    {
                        "name": "minForAddFee",
                        "type": "uint128"
                    },
                    {
                        "name": "forAddStampCost",
                        "type": "uint128"
                    },
                    {
                        "name": "endorseIncomePercent",
                        "type": "uint16"
                    }
                ]
            }
        ],
        "data": [],
        "events": [
            {
                "name": "TK_CT_nifi_stamp1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    }
                ],
                "outputs": []
            }
        ],
        "fields": [
            {
                "name": "_pubkey",
                "type": "uint256"
            },
            {
                "name": "_timestamp",
                "type": "uint64"
            },
            {
                "name": "_constructorFlag",
                "type": "bool"
            },
            {
                "name": "_manager",
                "type": "address"
            },
            {
                "name": "_creationTopup",
                "type": "uint128"
            },
            {
                "name": "_minCreationFee",
                "type": "uint128"
            },
            {
                "name": "_name",
                "type": "string"
            },
            {
                "name": "_symbol",
                "type": "string"
            },
            {
                "name": "_tokenCode",
                "type": "cell"
            },
            {
                "name": "_totalSupply",
                "type": "uint64"
            },
            {
                "name": "_minSealFee",
                "type": "uint128"
            },
            {
                "name": "_minSealRxFee",
                "type": "uint128"
            },
            {
                "name": "_requestEndorseFixIncome",
                "type": "uint128"
            },
            {
                "name": "_minForAddFee",
                "type": "uint128"
            },
            {
                "name": "_forAddStampCost",
                "type": "uint128"
            },
            {
                "name": "_endorseIncomePercent",
                "type": "uint16"
            }
        ]
    },
    tvc: "te6ccgECLAEABwwAAgE0AwEBAcACAEPQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgBCSK7VMg4wMgwP/jAiDA/uMC8gspBQQrA8LtRNDXScMB+GaJ+Gkh2zzTAAGOHIMI1xgg+QEB0wABlNP/AwGTAvhC4iD4ZfkQ8qiV0wAB8nri0z8B+EMhufK0IPgjgQPoqIIIG3dAoLnytPhj0x8B+CO88rnTHwHbPPI8IR8GA1LtRNDXScMB+GYi0NMD+kAw+GmpOADcIccA4wIh1w0f8rwh4wMB2zzyPCgoBgM8IIIQN5D+NrvjAiCCEGYM6RG74wIgghB+nWMWu+MCGg4HAzwgghB8lnYbuuMCIIIQfRifvbrjAiCCEH6dYxa64wIMCggDcjD4RvLgTPhCbuMA0z/R2zwhjh8j0NMB+kAwMcjPhyDOcc8LYQHIz5P6dYxazs3JcPsAkTDi4wDyACcJHABy+ELIy/9wbYBA9EP4KHFYgED0FgHIyz9yWIBA9EPI9ADJ+E/Iz4SA9AD0AM+ByfkAyM+KAEDL/8nQA0ow+Eby4Ez4Qm7jACGT1NHQ3tN/03/Tf9N/03/TD9HbPDDbPPIAJwslAE74SfhKxwXy4GYggScQu/LgZ/gAVQT4cVUD+HJVAvhzWPh0Afh1+HYDijD4RvLgTPhCbuMA0ds8Jo4rKNDTAfpAMDHIz4cgznHPC2FeUMjPk/JZ2G7Lf8t/y3/Lf8t/yw/NyXD7AJJfBuIw2zzyACcNJQAY+FH4UvhT+FT4VfhWBFAgghBZ4Kb0uuMCIIIQXx6CUbrjAiCCEGU8Q7u64wIgghBmDOkRuuMCFRMRDwNuMPhG8uBM+EJu4wDR2zwhjh8j0NMB+kAwMcjPhyDOcc8LYQHIz5OYM6RGzs3JcPsAkTDi4wDyACcQHAAE+EoDdDD4RvLgTPhCbuMA0ds8I44hJdDTAfpAMDHIz4cgzoBiz0BeEc+TlPEO7szMy3/JcPsAkl8D4uMA8gAnEhwADPhN+E74UAM6MPhG8uBM+EJu4wAhk9TR0N7Tf9N/0ds8MNs88gAnFCUAKvhJ+ErHBfLgZly88uBn+AAB+Gz4awOKMPhG8uBM+EJu4wAhldMf1NHQktMf4tP/0ds8IY4fI9DTAfpAMDHIz4cgznHPC2EByM+TZ4Kb0s7NyXD7AJEw4jDbPPIAJxYlAvxTEYEJYbny4RUwaKb+YPhMvvLhFvhQpLU/+HD4VvhV+FT4UvhRVRQB+Elw+Ekg+ELIy/9wbYBA9EP4KHFYgED0FvhQyMs/cliAQPRDyPQAyfhPyM+EgPQA9ADPgckg+QDIz4oAQMv/ydBVsPhLLcjPhYjOAfoCc88LaiHbPMwZFwGOz4NVoMjPkEUuuZLOVZDIzssfVXDIzssfy//Lf1UwyMt/y3/Lf8sPzc3Nzclw+wD4UNs8yM+HIM6CEAri9mbPC4HLP8lw+wAYAEiNCFgAiNq6GLmwcTnRuNEBIxngSnmyuAiJIuB5uWm44hkKiRQANNDSAAGT0gQx3tIAAZPSATHe9AT0BPQE0V8DBFAgghAZk6DOuuMCIIIQIamdqLrjAiCCEDcXk6m64wIgghA3kP42uuMCJCIeGwM8MPhG8uBM+EJu4wAhk9TR0N76QNN/0gDR2zzjAPIAJx0cACjtRNDT/9M/MfhDWMjL/8s/zsntVABC+En4SscF8uBm+AASyM+FgMoAz4RAzgH6AoBrz0DJcPsAAtgw+EJu4wD4RvJzIZPU0dDe+kDTf9N/1NTU0fhFIG6SMHDe+EK68uBl+ABVBPhqWPhtAfhu+G+CEAvrwgD4cYIQC+vCAPhyghAF9eEA+HOCEAvrwgD4dIIQBfXhAPh1gQH0+HYB+Gz4a9s88gAfJQIW7UTQ10nCAY6A4w0gJwJucO1E0PQFiXAgiF8gcF9g+Hb4dfh0+HP4cvhx+HD4b/hu+G34bPhr+GqAQPQO8r3XC//4YnD4YyErAEOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQA3Qw+Eby4Ez4Qm7jANHbPCKOISTQ0wH6QDAxyM+HIM6AYs9AXgHPkoamdqLLf8t/yXD7AJFb4jDbPPIAJyMlAAj4TPhLAzYw+Eby4Ez4Qm7jACGT1NHQ3vpA0ds8MNs88gAnJiUAhPhW+FX4VPhT+FL4UfhQ+E/4TvhN+Ez4S/hK+EP4QsjL/8s/z4POVbDIy3/Lf8zMzMs/y3/Lf8t/y3/Lf8sPzcntVAAa+En4SscF8uBm+AD4agCG7UTQ0//TP9MAMfpA1NHQ03/Tf9TU1NM/03/Tf9N/03/Tf9MP0fh2+HX4dPhz+HL4cfhw+G/4bvht+Gz4a/hq+GP4YgAK+Eby4EwCCvSkIPShKyoAFHNvbCAwLjYxLjAAAA==",
    code: "te6ccgECKQEABt8ABCSK7VMg4wMgwP/jAiDA/uMC8gsmAgEoA8LtRNDXScMB+GaJ+Gkh2zzTAAGOHIMI1xgg+QEB0wABlNP/AwGTAvhC4iD4ZfkQ8qiV0wAB8nri0z8B+EMhufK0IPgjgQPoqIIIG3dAoLnytPhj0x8B+CO88rnTHwHbPPI8HhwDA1LtRNDXScMB+GYi0NMD+kAw+GmpOADcIccA4wIh1w0f8rwh4wMB2zzyPCUlAwM8IIIQN5D+NrvjAiCCEGYM6RG74wIgghB+nWMWu+MCFwsEAzwgghB8lnYbuuMCIIIQfRifvbrjAiCCEH6dYxa64wIJBwUDcjD4RvLgTPhCbuMA0z/R2zwhjh8j0NMB+kAwMcjPhyDOcc8LYQHIz5P6dYxazs3JcPsAkTDi4wDyACQGGQBy+ELIy/9wbYBA9EP4KHFYgED0FgHIyz9yWIBA9EPI9ADJ+E/Iz4SA9AD0AM+ByfkAyM+KAEDL/8nQA0ow+Eby4Ez4Qm7jACGT1NHQ3tN/03/Tf9N/03/TD9HbPDDbPPIAJAgiAE74SfhKxwXy4GYggScQu/LgZ/gAVQT4cVUD+HJVAvhzWPh0Afh1+HYDijD4RvLgTPhCbuMA0ds8Jo4rKNDTAfpAMDHIz4cgznHPC2FeUMjPk/JZ2G7Lf8t/y3/Lf8t/yw/NyXD7AJJfBuIw2zzyACQKIgAY+FH4UvhT+FT4VfhWBFAgghBZ4Kb0uuMCIIIQXx6CUbrjAiCCEGU8Q7u64wIgghBmDOkRuuMCEhAODANuMPhG8uBM+EJu4wDR2zwhjh8j0NMB+kAwMcjPhyDOcc8LYQHIz5OYM6RGzs3JcPsAkTDi4wDyACQNGQAE+EoDdDD4RvLgTPhCbuMA0ds8I44hJdDTAfpAMDHIz4cgzoBiz0BeEc+TlPEO7szMy3/JcPsAkl8D4uMA8gAkDxkADPhN+E74UAM6MPhG8uBM+EJu4wAhk9TR0N7Tf9N/0ds8MNs88gAkESIAKvhJ+ErHBfLgZly88uBn+AAB+Gz4awOKMPhG8uBM+EJu4wAhldMf1NHQktMf4tP/0ds8IY4fI9DTAfpAMDHIz4cgznHPC2EByM+TZ4Kb0s7NyXD7AJEw4jDbPPIAJBMiAvxTEYEJYbny4RUwaKb+YPhMvvLhFvhQpLU/+HD4VvhV+FT4UvhRVRQB+Elw+Ekg+ELIy/9wbYBA9EP4KHFYgED0FvhQyMs/cliAQPRDyPQAyfhPyM+EgPQA9ADPgckg+QDIz4oAQMv/ydBVsPhLLcjPhYjOAfoCc88LaiHbPMwWFAGOz4NVoMjPkEUuuZLOVZDIzssfVXDIzssfy//Lf1UwyMt/y3/Lf8sPzc3Nzclw+wD4UNs8yM+HIM6CEAri9mbPC4HLP8lw+wAVAEiNCFgAiNq6GLmwcTnRuNEBIxngSnmyuAiJIuB5uWm44hkKiRQANNDSAAGT0gQx3tIAAZPSATHe9AT0BPQE0V8DBFAgghAZk6DOuuMCIIIQIamdqLrjAiCCEDcXk6m64wIgghA3kP42uuMCIR8bGAM8MPhG8uBM+EJu4wAhk9TR0N76QNN/0gDR2zzjAPIAJBoZACjtRNDT/9M/MfhDWMjL/8s/zsntVABC+En4SscF8uBm+AASyM+FgMoAz4RAzgH6AoBrz0DJcPsAAtgw+EJu4wD4RvJzIZPU0dDe+kDTf9N/1NTU0fhFIG6SMHDe+EK68uBl+ABVBPhqWPhtAfhu+G+CEAvrwgD4cYIQC+vCAPhyghAF9eEA+HOCEAvrwgD4dIIQBfXhAPh1gQH0+HYB+Gz4a9s88gAcIgIW7UTQ10nCAY6A4w0dJAJucO1E0PQFiXAgiF8gcF9g+Hb4dfh0+HP4cvhx+HD4b/hu+G34bPhr+GqAQPQO8r3XC//4YnD4Yx4oAEOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQA3Qw+Eby4Ez4Qm7jANHbPCKOISTQ0wH6QDAxyM+HIM6AYs9AXgHPkoamdqLLf8t/yXD7AJFb4jDbPPIAJCAiAAj4TPhLAzYw+Eby4Ez4Qm7jACGT1NHQ3vpA0ds8MNs88gAkIyIAhPhW+FX4VPhT+FL4UfhQ+E/4TvhN+Ez4S/hK+EP4QsjL/8s/z4POVbDIy3/Lf8zMzMs/y3/Lf8t/y3/Lf8sPzcntVAAa+En4SscF8uBm+AD4agCG7UTQ0//TP9MAMfpA1NHQ03/Tf9TU1NM/03/Tf9N/03/Tf9MP0fh2+HX4dPhz+HL4cfhw+G/4bvht+Gz4a/hq+GP4YgAK+Eby4EwCCvSkIPShKCcAFHNvbCAwLjYxLjAAAA==",
    codeHash: "efc1ec8a1dbdbe5ca9994cec3dffd3ce82c7cc83d95cb764374ddbba4422bca4",
};
module.exports = { StampRootContract };