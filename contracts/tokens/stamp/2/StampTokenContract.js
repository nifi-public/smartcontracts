const StampTokenContract = {
    abi: {
        "ABI version": 2,
        "version": "2.2",
        "header": [
            "pubkey",
            "time",
            "expire"
        ],
        "functions": [
            {
                "name": "constructor",
                "inputs": [
                    {
                        "name": "owner",
                        "type": "address"
                    },
                    {
                        "name": "manager",
                        "type": "address"
                    },
                    {
                        "name": "managerUnlockTime",
                        "type": "uint32"
                    },
                    {
                        "name": "creator",
                        "type": "address"
                    },
                    {
                        "name": "creatorPercent",
                        "type": "uint32"
                    },
                    {
                        "name": "hash",
                        "type": "uint256"
                    },
                    {
                        "name": "minRequestEndorseFee",
                        "type": "uint128"
                    },
                    {
                        "name": "minCancelEndorseFee",
                        "type": "uint128"
                    },
                    {
                        "name": "minForAddFee",
                        "type": "uint128"
                    },
                    {
                        "name": "forAddStampCost",
                        "type": "uint128"
                    },
                    {
                        "name": "endorseIncomePercent",
                        "type": "uint16"
                    }
                ],
                "outputs": []
            },
            {
                "name": "changeOwner",
                "inputs": [
                    {
                        "name": "owner",
                        "type": "address"
                    }
                ],
                "outputs": []
            },
            {
                "name": "receiveArtInfo",
                "inputs": [
                    {
                        "name": "answerId",
                        "type": "uint32"
                    }
                ],
                "outputs": [
                    {
                        "name": "creator",
                        "type": "address"
                    },
                    {
                        "name": "creatorPercent",
                        "type": "uint32"
                    },
                    {
                        "name": "hash",
                        "type": "uint256"
                    }
                ]
            },
            {
                "name": "getArtInfo",
                "inputs": [],
                "outputs": [
                    {
                        "name": "creator",
                        "type": "address"
                    },
                    {
                        "name": "creatorPercent",
                        "type": "uint32"
                    },
                    {
                        "name": "hash",
                        "type": "uint256"
                    }
                ]
            },
            {
                "name": "getSeal",
                "inputs": [],
                "outputs": [
                    {
                        "name": "seal",
                        "type": "optional(address)"
                    },
                    {
                        "name": "corner",
                        "type": "uint8"
                    }
                ]
            },
            {
                "name": "getForever",
                "inputs": [],
                "outputs": [
                    {
                        "name": "forever",
                        "type": "optional(address)"
                    }
                ]
            },
            {
                "name": "getInfo",
                "inputs": [],
                "outputs": [
                    {
                        "name": "root",
                        "type": "address"
                    },
                    {
                        "name": "id",
                        "type": "uint64"
                    }
                ]
            },
            {
                "name": "receiveTradeInfo",
                "inputs": [
                    {
                        "name": "answerId",
                        "type": "uint32"
                    }
                ],
                "outputs": [
                    {
                        "name": "owner",
                        "type": "address"
                    },
                    {
                        "name": "creator",
                        "type": "address"
                    },
                    {
                        "name": "creatorPercent",
                        "type": "uint32"
                    },
                    {
                        "name": "manager",
                        "type": "address"
                    },
                    {
                        "name": "managerUnlockTime",
                        "type": "uint32"
                    }
                ]
            },
            {
                "name": "getTradeInfo",
                "inputs": [],
                "outputs": [
                    {
                        "name": "owner",
                        "type": "address"
                    },
                    {
                        "name": "creator",
                        "type": "address"
                    },
                    {
                        "name": "creatorPercent",
                        "type": "uint32"
                    },
                    {
                        "name": "manager",
                        "type": "address"
                    },
                    {
                        "name": "managerUnlockTime",
                        "type": "uint32"
                    }
                ]
            },
            {
                "name": "lockManager",
                "inputs": [
                    {
                        "name": "manager",
                        "type": "address"
                    },
                    {
                        "name": "unlockTime",
                        "type": "uint32"
                    }
                ],
                "outputs": []
            },
            {
                "name": "unlock",
                "inputs": [],
                "outputs": []
            },
            {
                "name": "requestEndorse",
                "inputs": [
                    {
                        "name": "seal",
                        "type": "address"
                    },
                    {
                        "name": "places",
                        "type": "uint8"
                    },
                    {
                        "name": "price",
                        "type": "uint128"
                    }
                ],
                "outputs": []
            },
            {
                "name": "cancelEndorse",
                "inputs": [],
                "outputs": []
            },
            {
                "name": "endorse",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "place",
                        "type": "uint8"
                    },
                    {
                        "name": "sealOwner",
                        "type": "address"
                    }
                ],
                "outputs": []
            },
            {
                "name": "setForever",
                "inputs": [
                    {
                        "name": "forever",
                        "type": "address"
                    }
                ],
                "outputs": []
            },
            {
                "name": "delForever",
                "inputs": [],
                "outputs": []
            },
            {
                "name": "getParameters",
                "inputs": [],
                "outputs": [
                    {
                        "name": "minRequestEndorseFee",
                        "type": "uint128"
                    },
                    {
                        "name": "minCancelEndorseFee",
                        "type": "uint128"
                    },
                    {
                        "name": "minForAddFee",
                        "type": "uint128"
                    },
                    {
                        "name": "forAddStampCost",
                        "type": "uint128"
                    }
                ]
            }
        ],
        "data": [
            {
                "key": 1,
                "name": "_root",
                "type": "address"
            },
            {
                "key": 2,
                "name": "_id",
                "type": "uint64"
            }
        ],
        "events": [
            {
                "name": "TK_CO_nifi_stamp1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "newOwner",
                        "type": "address"
                    }
                ],
                "outputs": []
            },
            {
                "name": "TK_MG_nifi_stamp1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "newManager",
                        "type": "address"
                    },
                    {
                        "name": "expirationTime",
                        "type": "uint32"
                    }
                ],
                "outputs": []
            },
            {
                "name": "TK_RQ_nifi_stamp1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "seal",
                        "type": "address"
                    },
                    {
                        "name": "sealPlaces",
                        "type": "uint8"
                    },
                    {
                        "name": "value",
                        "type": "uint64"
                    }
                ],
                "outputs": []
            },
            {
                "name": "TK_RX_nifi_stamp1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    }
                ],
                "outputs": []
            },
            {
                "name": "TK_EN_nifi_stamp1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "seal",
                        "type": "address"
                    },
                    {
                        "name": "corner",
                        "type": "uint8"
                    }
                ],
                "outputs": []
            },
            {
                "name": "TK_FD_nifi_stamp1",
                "inputs": [
                    {
                        "name": "id",
                        "type": "uint64"
                    },
                    {
                        "name": "forever",
                        "type": "address"
                    }
                ],
                "outputs": []
            }
        ],
        "fields": [
            {
                "name": "_pubkey",
                "type": "uint256"
            },
            {
                "name": "_timestamp",
                "type": "uint64"
            },
            {
                "name": "_constructorFlag",
                "type": "bool"
            },
            {
                "name": "_root",
                "type": "address"
            },
            {
                "name": "_id",
                "type": "uint64"
            },
            {
                "name": "_owner",
                "type": "address"
            },
            {
                "name": "_manager",
                "type": "address"
            },
            {
                "name": "_managerUnlockTime",
                "type": "uint32"
            },
            {
                "name": "_creator",
                "type": "address"
            },
            {
                "name": "_creatorPercent",
                "type": "uint32"
            },
            {
                "name": "_hash",
                "type": "uint256"
            },
            {
                "name": "_sealPlace",
                "type": "uint8"
            },
            {
                "name": "_seal",
                "type": "optional(address)"
            },
            {
                "name": "_sealValue",
                "type": "uint128"
            },
            {
                "name": "_sealPosiblePlaces",
                "type": "uint8"
            },
            {
                "name": "_forever",
                "type": "optional(address)"
            },
            {
                "name": "_minRequestEndorseFee",
                "type": "uint128"
            },
            {
                "name": "_minCancelEndorseFee",
                "type": "uint128"
            },
            {
                "name": "_minForAddFee",
                "type": "uint128"
            },
            {
                "name": "_forAddStampCost",
                "type": "uint128"
            },
            {
                "name": "_endorseIncomePercent",
                "type": "uint16"
            }
        ]
    },
    tvc: "te6ccgECSAEADScAAgE0AwEBAcACAEPQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgBCSK7VMg4wMgwP/jAiDA/uMC8gtFBQRHA7ztRNDXScMB+GaJ+Gkh2zzTAAGOGYMI1xgg+QEB0wABlNP/AwGTAvhC4vkQ8qiV0wAB8nri0z8B+EMhufK0IPgjgQPoqIIIG3dAoLnytPhj0x8B+CO88rnTHwHbPPI8OTYGBHztRNDXScMB+GYi0NMD+kAw+GmpOAD4RH9vcYIImJaAb3Jtb3Nwb3T4ZOMCIccA4wIh1w0f8rwh4wMB2zzyPEA/PwYCKCCCEGwfT7C74wIgghBv2nP+uuMCCgcDijD4RvLgTPhCbuMA0x/4RFhvdfhk0ds8JY4rJ9DTAfpAMDHIz4cgznHPC2FeQMjPk79pz/rOVTDIzssfWcjOyx/Nzc3JcEQJCAGQjj/4RCBvEyFvEvhJVQJvEcjPhIDKAM+EQM4B+gL0AHHPC2leQMj4RG8VzwsfzlUwyM7LH1nIzssfzc3NyfhEbxTi+wDjAPIAOwEg+ERwb3Jwb3GAQG90+GTbPC0EUCCCEBJgG5W74wIgghAszSSfu+MCIIIQRWQ8aLvjAiCCEGwfT7C74wIuIxQLBFAgghBRYzgBuuMCIIIQXMTmtrrjAiCCEF+Th9W64wIgghBsH0+wuuMCEg8NDAN6MPhG8uBM+EJu4wDR2zwjjiQl0NMB+kAwMcjPhyDOcc8LYV4gyM+TsH0+ws7LH8v/zclw+wCSXwPi4wDyAEQrOwMmMPhG8uBM+EJu4wDR2zww2zzyAEQOQQFu+FZusyCbMPhJ+FYgbvJ/xwXe8uB2bfh2+En4S9s8yM+HIM5xzwthWcjPkH0DVpLLP87NyXD7AEMDJjD4RvLgTPhCbuMA0ds8MNs88gBEEEEBMoj4Vm74SfhNxwX4I/hOubCw8uhn+ABw+G4RADxNZXRob2QgZm9yIGxvY2tlZCBtYW5hZ2VyIG9ubHkDJjD4RvLgTPhCbuMA0ds8MNs88gBEE0EC0Ij4Vm74SfhMxwX4I/hOvrCw8uhs+FLy0G74U27y0G9opv5g+Fi+8uBwbfhz+FT4TMjPhYjOAfoCgGvPQMlx+wD4S9s8yM+HIM6CEGVPhKTPC4HLP8lw+wD4SsjPhYjOgG/PQMmAQPsAIkMEUCCCEDiiV6664wIgghA8g+XguuMCIIIQP+eXrrrjAiCCEEVkPGi64wIgGhgVAzow+Eby4Ez4Qm7jACGT1NHQ3vpA0x/R2zww2zzyAEQWQQTCiPhWbvhJ+EzHBfgj+E6+sLD4SfhNxwX4I/hOubCx8uhoiCL6Qm8T1wv/wwDy6GqI+CMiufLoafgAAfhtIPhu+E34S9s8yM+HIM5xzwthVSDIz5BYCTYCyz/Oyx/NyXD7ACgzF0MAIkludmFsaWQgbG9jayB0aW1lAzYw+Eby4Ez4Qm7jACGT1NHQ3vpA0ds8MNs88gBEGUEB9oj4Vm74SfhMxwX4I/hOvrCw8uhsaKb+YPhZvvLgcPhWbvLgc/hTbvLQcvhS8uB0IPh2+FL4UyBu8n/4TPhL+FpVBH/Iz4WAygDPhEDOAfoCcc8LalUwyM+Qi4gIvss/zlnIzssHzc3JcPsA+ErIz4WIzoBvz0DJgED7ACIDSDD4RvLgTPhCbuMAIZfTP9MH1NHQlNM/0wfi+kDR2zww2zzyAEQbQQLI+FNusyCbMPhJ+FMgbvJ/xwXe8uBvIfhVsPLgcYLw+TujEcqaK6wMSkor8/hTq7lmGNasVkWGLaq0oq/6ABHIy/9wbYBA9EPbPHFYgED0FlUCyMs/cliAQPRDyPQAySD5AAHXZR8cA9KC8I4h4XCvAoz3ZaSgzUIAqnEsia0GTDqD2Vp/vIWqg0ETWHhVAts8+EkByM+KAEDL/8nQxwXy4Gr4AAEg+HL4UyBu8n/4S9s8yM+HIM5xzwthVSDIz5Fp2Xtuyz/OywfNyXD7APhbwgAeQx0AkI4v+FT4W4EnEKmEtX/4VKK1fyHIz4WIzgH6AoBrz0DJcfsA+ErIz4WIzoBvz0DJgECOEfhUIcjPhYjOAfoCgGvPQMlx4vsAMAAsyM+MCATSWM8LD8sPWM8L/8v/ydD5AgBIjQhgATayAqnVEj0Uj6mTD+s82dQfMiMeo4Co4MJYYi9c9cxUAz4w+Eby4Ez4Qm7jACGT1NHQ3vpA0wfTf9HbPDDbPPIARCFBAuCI+FZu+En4TMcF+CP4Tr6wsPLobGim/mD4VyKgtX++8uBt+FLy0G4gdPsCWPhz+HQg+HX4VLU/AfhTIG7yf/hL2zzIz4cgznHPC2FVMMjPkPpqnFLLP87LB8s/zclw+wD4SsjPhYjOgG/PQMmDBvsAIkMAKk1ldGhvZCBmb3Igb3duZXIgb25seQRQIIIQFHsIyLrjAiCCEBWltZS64wIgghAhezMIuuMCIIIQLM0kn7rjAiwpJiQDcjD4RvLgTPhCbuMA0ds8Io4hJNDTAfpAMDHIz4cgznHPC2ECyM+SszSSfs7LP83JcPsAkVvi4wDyAEQlOwAI+Er4SwM2MPhG8uBM+EJu4wAhk9TR0N76QNHbPDDbPPIARCdBA6KI+FZu+En4TMcF+CP4Tr6wsPhJ+E3HBfgj+E65sLHy6GiIIfpCbxPXC//DAPLoavgAIPhs+EvbPMjPhyDOcc8LYVnIz5BIDxKGyz/Ozclw+wAoM0MASE1ldGhvZCBmb3IgdGhlIG93bmVyIG9yIG1hbmFnZXIgb25seQP2MPhG8uBM+EJu4wDTH/hEWG91+GTR2zwjjiIl0NMB+kAwMcjPhyDOcc8LYV4gyM+SVpbWUs7LH8v/zclwjjb4RCBvEyFvEvhJVQJvEcjPhIDKAM+EQM4B+gL0AHHPC2leIMj4RG8Vzwsfzssfy//NyfhEbxTi+wDjAPIARCo7ASD4RHBvcnBvcYBAb3T4ZNs8KwAM+E/4UPhRA4ww+Eby4Ez4Qm7jANHbPCWOLSfQ0wH6QDAxyM+HIM5xzwthXkDIz5JR7CMizlUwyM7LH1nIzssfzc3NyXD7AJJfBeLjAPIARC07ABT4TPhP+FD4TfhOBEwgggkxnzq64wIgggsnHNu64wIgghARS65kuuMCIIIQEmAblbrjAj06MS8DijD4RvLgTPhCbuMA0ds8Io4tJNDTAfpAMDHIz4cgznHPC2ECyM+SSYBuVgEgbpMwz4GUAc+DzuLLB83JcPsAkVvi4wDyAEQwOwAI+FP4UgSGMPhCbuMA+EbycyGT1NHQ3vpA1NHQ+kDTH9TR0PpA0x/T/9N/1NHQ03/Tf9N/0w/RiPhJ+ErHBfLoZYgngQlhufLoazY1NDIDlIgo+kJvE9cL/8MA8uhqiCv6Qm8T1wv/wwDy6Gr4AFUG+G9VBfhwVQT4cVUG+GxVBfhtVQT4blUD+HdVAvh4WPh5Afh6+HvbPPIAMzNBACpBZGRyZXNzIGNhbid0IGJlIG51bGwAKFVudmFsaWQgY3JlYXRvciBmZWVzADBNZXRob2QgZm9yIHRoZSByb290IG9ubHkCFu1E0NdJwgGOgOMNN0QDpnDtRND0BXD4QPhB+EL4Q/hE+EX4RvhH+Ej4SXErgED0Do6A33IsgED0DpPXCz+RcOKJIHCJcF8gbXAgbXBfQIAcb4DtV4BA9A7yvdcL//hicPhjODk5AQKJOQBDgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAOGMPhG8uBM+EJu4wDR2zwhjisj0NMB+kAwMcjPhyDOcc8LYQHIz5IMnHNuASBukzDPgZQBz4PO4s3JcPsAkTDi4wDyAEQ8OwAo7UTQ0//TPzH4Q1jIy//LP87J7VQABPhWA4Iw+Eby4Ez4Qm7jANHbPCSOJybQ0wH6QDAxyM+HIM5xzwthXjDIz5IExnzqy3/Lf8t/y3/NyXD7AJJfBOIw2zzyAEQ+QQAQ+Ff4WPhZ+FoACvhG8uBMA0wh1h8xMPhG8uBM+EJu4wD4Vm6zIJsw+FYgbvJ/+EnHBd6OgN7bPERCQQDa7UdwgBxvh4Adb4IwgBxwZF8K+EP4QsjL/8s/z4POyz9V8MjOVeDIzssfVcDIzssfy//LB1WAyAEgbpMwz4GUAc+DzuLLf8sHVVDIASBukzDPgZQBz4PO4st/y3/Lf1nIy3/LD83Nzc3NzcntVAFEbfh2+En4S9s8yM+HIM5xzwthWcjPkH0DVpLLP87NyXD7AEMASI0IWACI2roYubBxOdG40QEjGeBKebK4CIki4Hm5abjiGQqJFADo7UTQ0//TP9MAMfpA0z/U0dD6QNTR0PpA0x/U0dD6QNMf0//TB9TR0NIAAZL6QJJtAeLTf9MH1NHQ0gABkvpAkm0B4tN/03/Tf9TR0NN/0w/RcPhA+EH4QvhD+ET4RfhG+Ef4SPhJgBJ6Y4Acb4DtV/hj+GICCvSkIPShR0YAFHNvbCAwLjYxLjAAAA==",
    code: "te6ccgECRQEADPoABCSK7VMg4wMgwP/jAiDA/uMC8gtCAgFEA7ztRNDXScMB+GaJ+Gkh2zzTAAGOGYMI1xgg+QEB0wABlNP/AwGTAvhC4vkQ8qiV0wAB8nri0z8B+EMhufK0IPgjgQPoqIIIG3dAoLnytPhj0x8B+CO88rnTHwHbPPI8NjMDBHztRNDXScMB+GYi0NMD+kAw+GmpOAD4RH9vcYIImJaAb3Jtb3Nwb3T4ZOMCIccA4wIh1w0f8rwh4wMB2zzyPD08PAMCKCCCEGwfT7C74wIgghBv2nP+uuMCBwQDijD4RvLgTPhCbuMA0x/4RFhvdfhk0ds8JY4rJ9DTAfpAMDHIz4cgznHPC2FeQMjPk79pz/rOVTDIzssfWcjOyx/Nzc3JcEEGBQGQjj/4RCBvEyFvEvhJVQJvEcjPhIDKAM+EQM4B+gL0AHHPC2leQMj4RG8VzwsfzlUwyM7LH1nIzssfzc3NyfhEbxTi+wDjAPIAOAEg+ERwb3Jwb3GAQG90+GTbPCoEUCCCEBJgG5W74wIgghAszSSfu+MCIIIQRWQ8aLvjAiCCEGwfT7C74wIrIBEIBFAgghBRYzgBuuMCIIIQXMTmtrrjAiCCEF+Th9W64wIgghBsH0+wuuMCDwwKCQN6MPhG8uBM+EJu4wDR2zwjjiQl0NMB+kAwMcjPhyDOcc8LYV4gyM+TsH0+ws7LH8v/zclw+wCSXwPi4wDyAEEoOAMmMPhG8uBM+EJu4wDR2zww2zzyAEELPgFu+FZusyCbMPhJ+FYgbvJ/xwXe8uB2bfh2+En4S9s8yM+HIM5xzwthWcjPkH0DVpLLP87NyXD7AEADJjD4RvLgTPhCbuMA0ds8MNs88gBBDT4BMoj4Vm74SfhNxwX4I/hOubCw8uhn+ABw+G4OADxNZXRob2QgZm9yIGxvY2tlZCBtYW5hZ2VyIG9ubHkDJjD4RvLgTPhCbuMA0ds8MNs88gBBED4C0Ij4Vm74SfhMxwX4I/hOvrCw8uhs+FLy0G74U27y0G9opv5g+Fi+8uBwbfhz+FT4TMjPhYjOAfoCgGvPQMlx+wD4S9s8yM+HIM6CEGVPhKTPC4HLP8lw+wD4SsjPhYjOgG/PQMmAQPsAH0AEUCCCEDiiV6664wIgghA8g+XguuMCIIIQP+eXrrrjAiCCEEVkPGi64wIdFxUSAzow+Eby4Ez4Qm7jACGT1NHQ3vpA0x/R2zww2zzyAEETPgTCiPhWbvhJ+EzHBfgj+E6+sLD4SfhNxwX4I/hOubCx8uhoiCL6Qm8T1wv/wwDy6GqI+CMiufLoafgAAfhtIPhu+E34S9s8yM+HIM5xzwthVSDIz5BYCTYCyz/Oyx/NyXD7ACUwFEAAIkludmFsaWQgbG9jayB0aW1lAzYw+Eby4Ez4Qm7jACGT1NHQ3vpA0ds8MNs88gBBFj4B9oj4Vm74SfhMxwX4I/hOvrCw8uhsaKb+YPhZvvLgcPhWbvLgc/hTbvLQcvhS8uB0IPh2+FL4UyBu8n/4TPhL+FpVBH/Iz4WAygDPhEDOAfoCcc8LalUwyM+Qi4gIvss/zlnIzssHzc3JcPsA+ErIz4WIzoBvz0DJgED7AB8DSDD4RvLgTPhCbuMAIZfTP9MH1NHQlNM/0wfi+kDR2zww2zzyAEEYPgLI+FNusyCbMPhJ+FMgbvJ/xwXe8uBvIfhVsPLgcYLw+TujEcqaK6wMSkor8/hTq7lmGNasVkWGLaq0oq/6ABHIy/9wbYBA9EPbPHFYgED0FlUCyMs/cliAQPRDyPQAySD5AAHXZRwZA9KC8I4h4XCvAoz3ZaSgzUIAqnEsia0GTDqD2Vp/vIWqg0ETWHhVAts8+EkByM+KAEDL/8nQxwXy4Gr4AAEg+HL4UyBu8n/4S9s8yM+HIM5xzwthVSDIz5Fp2Xtuyz/OywfNyXD7APhbwgAbQBoAkI4v+FT4W4EnEKmEtX/4VKK1fyHIz4WIzgH6AoBrz0DJcfsA+ErIz4WIzoBvz0DJgECOEfhUIcjPhYjOAfoCgGvPQMlx4vsAMAAsyM+MCATSWM8LD8sPWM8L/8v/ydD5AgBIjQhgATayAqnVEj0Uj6mTD+s82dQfMiMeo4Co4MJYYi9c9cxUAz4w+Eby4Ez4Qm7jACGT1NHQ3vpA0wfTf9HbPDDbPPIAQR4+AuCI+FZu+En4TMcF+CP4Tr6wsPLobGim/mD4VyKgtX++8uBt+FLy0G4gdPsCWPhz+HQg+HX4VLU/AfhTIG7yf/hL2zzIz4cgznHPC2FVMMjPkPpqnFLLP87LB8s/zclw+wD4SsjPhYjOgG/PQMmDBvsAH0AAKk1ldGhvZCBmb3Igb3duZXIgb25seQRQIIIQFHsIyLrjAiCCEBWltZS64wIgghAhezMIuuMCIIIQLM0kn7rjAikmIyEDcjD4RvLgTPhCbuMA0ds8Io4hJNDTAfpAMDHIz4cgznHPC2ECyM+SszSSfs7LP83JcPsAkVvi4wDyAEEiOAAI+Er4SwM2MPhG8uBM+EJu4wAhk9TR0N76QNHbPDDbPPIAQSQ+A6KI+FZu+En4TMcF+CP4Tr6wsPhJ+E3HBfgj+E65sLHy6GiIIfpCbxPXC//DAPLoavgAIPhs+EvbPMjPhyDOcc8LYVnIz5BIDxKGyz/Ozclw+wAlMEAASE1ldGhvZCBmb3IgdGhlIG93bmVyIG9yIG1hbmFnZXIgb25seQP2MPhG8uBM+EJu4wDTH/hEWG91+GTR2zwjjiIl0NMB+kAwMcjPhyDOcc8LYV4gyM+SVpbWUs7LH8v/zclwjjb4RCBvEyFvEvhJVQJvEcjPhIDKAM+EQM4B+gL0AHHPC2leIMj4RG8Vzwsfzssfy//NyfhEbxTi+wDjAPIAQSc4ASD4RHBvcnBvcYBAb3T4ZNs8KAAM+E/4UPhRA4ww+Eby4Ez4Qm7jANHbPCWOLSfQ0wH6QDAxyM+HIM5xzwthXkDIz5JR7CMizlUwyM7LH1nIzssfzc3NyXD7AJJfBeLjAPIAQSo4ABT4TPhP+FD4TfhOBEwgggkxnzq64wIgggsnHNu64wIgghARS65kuuMCIIIQEmAblbrjAjo3LiwDijD4RvLgTPhCbuMA0ds8Io4tJNDTAfpAMDHIz4cgznHPC2ECyM+SSYBuVgEgbpMwz4GUAc+DzuLLB83JcPsAkVvi4wDyAEEtOAAI+FP4UgSGMPhCbuMA+EbycyGT1NHQ3vpA1NHQ+kDTH9TR0PpA0x/T/9N/1NHQ03/Tf9N/0w/RiPhJ+ErHBfLoZYgngQlhufLoazMyMS8DlIgo+kJvE9cL/8MA8uhqiCv6Qm8T1wv/wwDy6Gr4AFUG+G9VBfhwVQT4cVUG+GxVBfhtVQT4blUD+HdVAvh4WPh5Afh6+HvbPPIAMDA+ACpBZGRyZXNzIGNhbid0IGJlIG51bGwAKFVudmFsaWQgY3JlYXRvciBmZWVzADBNZXRob2QgZm9yIHRoZSByb290IG9ubHkCFu1E0NdJwgGOgOMNNEEDpnDtRND0BXD4QPhB+EL4Q/hE+EX4RvhH+Ej4SXErgED0Do6A33IsgED0DpPXCz+RcOKJIHCJcF8gbXAgbXBfQIAcb4DtV4BA9A7yvdcL//hicPhjNTY2AQKJNgBDgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAOGMPhG8uBM+EJu4wDR2zwhjisj0NMB+kAwMcjPhyDOcc8LYQHIz5IMnHNuASBukzDPgZQBz4PO4s3JcPsAkTDi4wDyAEE5OAAo7UTQ0//TPzH4Q1jIy//LP87J7VQABPhWA4Iw+Eby4Ez4Qm7jANHbPCSOJybQ0wH6QDAxyM+HIM5xzwthXjDIz5IExnzqy3/Lf8t/y3/NyXD7AJJfBOIw2zzyAEE7PgAQ+Ff4WPhZ+FoACvhG8uBMA0wh1h8xMPhG8uBM+EJu4wD4Vm6zIJsw+FYgbvJ/+EnHBd6OgN7bPEE/PgDa7UdwgBxvh4Adb4IwgBxwZF8K+EP4QsjL/8s/z4POyz9V8MjOVeDIzssfVcDIzssfy//LB1WAyAEgbpMwz4GUAc+DzuLLf8sHVVDIASBukzDPgZQBz4PO4st/y3/Lf1nIy3/LD83Nzc3NzcntVAFEbfh2+En4S9s8yM+HIM5xzwthWcjPkH0DVpLLP87NyXD7AEAASI0IWACI2roYubBxOdG40QEjGeBKebK4CIki4Hm5abjiGQqJFADo7UTQ0//TP9MAMfpA0z/U0dD6QNTR0PpA0x/U0dD6QNMf0//TB9TR0NIAAZL6QJJtAeLTf9MH1NHQ0gABkvpAkm0B4tN/03/Tf9TR0NN/0w/RcPhA+EH4QvhD+ET4RfhG+Ef4SPhJgBJ6Y4Acb4DtV/hj+GICCvSkIPShREMAFHNvbCAwLjYxLjAAAA==",
    codeHash: "4a6ea7eadf31804146f8d8d83b7434755c3532fa4b78f629111b852a41c874d6",
};
module.exports = { StampTokenContract };