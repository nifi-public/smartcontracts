pragma ton-solidity >=0.47.0; 

    library SealContractInfo {
        function SEAL_ROOT() internal inline returns(address){
            return address(0x26d640553aa247a291f53261fd679b3a83e64463d470151c184b0c45eb9eb98a);
        }
        uint256 constant SEAL_ROOT_PUBKEY = 0xf93ba311ca9a2bac0c4a4a2bf3f853abb96618d6ac5645862daab4a2affa0011;
        uint256 constant SEAL_CODEHASH = 0x8e21e170af028cf765a4a0cd4200aa712c89ad064c3a83d95a7fbc85aa834113;
        uint16 constant  SEAL_CODEDEPTH = 8;
    }