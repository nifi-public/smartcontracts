pragma ton-solidity >=0.47.0;
pragma AbiHeader expire;
pragma AbiHeader time;
pragma AbiHeader pubkey;
// import required DeBot interfaces and basic DeBot contract.
import "https://raw.githubusercontent.com/tonlabs/debots/main/Debot.sol";
import "https://raw.githubusercontent.com/tonlabs/DeBot-IS-consortium/main/UserInfo/UserInfo.sol";
import "https://raw.githubusercontent.com/tonlabs/DeBot-IS-consortium/main/Terminal/Terminal.sol";
import "https://raw.githubusercontent.com/tonlabs/DeBot-IS-consortium/main/ConfirmInput/ConfirmInput.sol";
import "https://raw.githubusercontent.com/tonlabs/DeBot-IS-consortium/main/Base64/Base64.sol";
import "https://raw.githubusercontent.com/tonlabs/DeBot-IS-consortium/main/Hex/Hex.sol";
import "https://raw.githubusercontent.com/tonlabs/DeBot-IS-consortium/main/Network/Network.sol";
import "https://raw.githubusercontent.com/tonlabs/DeBot-IS-consortium/main/Sdk/Sdk.sol";
import "https://raw.githubusercontent.com/tonlabs/DeBot-IS-consortium/main/Json/Json.sol";
import "Upgradable.sol";

abstract contract AMSig {
    function sendTransaction(
        address dest,
        uint128 value,
        bool bounce,
        uint8 flag,
        TvmCell payload)
    public {}
    function submitTransaction(
        address dest,
        uint128 value,
        bool bounce,
        bool allBalance,
        TvmCell payload)
    public returns (uint64 transId) {}
}

struct JsonData {
    bool success;
    bytes data;
}

contract SurfAuthDebot is Debot, Upgradable {

    string constant GET_URL = "https://dev.nifi.club/api/auth/signdata/";
    string constant POST_URL = "https://dev.nifi.club/api/auth/sign";

    uint256 m_pk;
    uint32 m_sign;
    address m_dst;
    string m_returnUrl;
    bytes m_icon;
    TvmCell m_sendMsg;
    uint128 m_amount;
    TvmCell m_payload;
    address m_sender;

    function setIcon(bytes icon) public {
        require(msg.pubkey()==tvm.pubkey(),101);
        tvm.accept();
        m_icon = icon;
    }

    function setABIBytes(bytes dabi) public {
        require(tvm.pubkey() == msg.pubkey(), 100);
        tvm.accept();
        m_options |= DEBOT_ABI;
        m_debotAbi = dabi;
    }

    function getAuthMsg(string returnUrl) public pure returns(TvmCell message) {
        TvmCell body = tvm.encodeBody(SurfAuthDebot.auth, returnUrl);
        TvmBuilder message_;
        message_.store(false, true, true, false, address(0), address(this));
        message_.storeTons(0);
        message_.storeUnsigned(0, 1);
        message_.storeTons(0);
        message_.storeTons(0);
        message_.store(uint64(0));
        message_.store(uint32(0));
        message_.storeUnsigned(0, 1); //init: nothing$0
        message_.storeUnsigned(1, 1); //body: right$1
        message_.store(body);
        message = message_.toCell();
    }

    function start() public override {
        Terminal.print(0,"Invoke me!");
    }

    function auth(string returnUrl) public {
        m_returnUrl = returnUrl;
        UserInfo.getSigningBox(tvm.functionId(getPostUserSign));
    }

    function getPostUserSign(uint32 handle) public {
        m_sign = handle;
        UserInfo.getAccount(tvm.functionId(getPostUserAddress));
    }

    function getPostUserAddress (address value) public {
        m_dst = value;
        UserInfo.getPublicKey(tvm.functionId(getPostPubkey));
    }
    function getPostPubkey (uint256 value) public {
        m_pk = value;
        string url = GET_URL;
        url.append(format("{}",m_dst));
        string[] headers;
        headers.push("Content-Type: application/json");
        Network.get(tvm.functionId(getDataToSign), url, headers);
    }

    function printError() public {
        Terminal.print(0,'Authentication FAILED. Please try again later.');
    }

    function getDataToSign(int32 statusCode, string[] retHeaders, string content) public {
        retHeaders;
        content;
        if (statusCode == 200) {
            Json.deserialize(tvm.functionId(getDataToSignJson),content);
        } else {
            printError();
        }
    }

    function getDataToSignJson(bool result, JsonData obj)public {
        if (obj.success) {
            Base64.decode(tvm.functionId(decodeBase64),obj.data);
        } else {
            printError();
        }
    }

    function decodeBase64(bytes data) public {
        uint256 hash = data.toSlice().decode(uint256);
        Sdk.signHash(tvm.functionId(setSignature), m_sign, hash);
    }

    function setSignature(bytes signature) public {
        Hex.encode(tvm.functionId(setEncode), signature);
    }

    function setEncode(string  hexstr) public {
        string[] headers;
        headers.push("Content-Type: application/json");
        string body = "{\"signature\":\"";
        body.append(hexstr);
        body.append("\",");
        body.append(format("\"walletAddress\":\"{}\"}",m_dst));
        headers.push(format("Content-Length: {}",body.byteLength()));
        Network.post(tvm.functionId(setPostResponse), POST_URL, headers, body);
    }

    function setPostResponse(int32 statusCode, string[] retHeaders, string content) public {
        retHeaders;
        content;
        if (statusCode == 200) {
            Json.deserialize(tvm.functionId(getDataToMoveJson),content);
        } else {
            printError();
        }
    }

    function getDataToMoveJson(bool result, JsonData obj)public {
        if (obj.success) {
            string returnUrl;
            optional(uint32) pos = m_returnUrl.find("{token}");
            if (pos.hasValue()) {
                string url;
                url = m_returnUrl.substr(0,pos.get());
                url.append(obj.data);
                url.append(m_returnUrl.substr(pos.get()+7));
                optional(uint32) pos1 = url.find("{walletAddress}");
                if (pos1.hasValue()) {
                    returnUrl = url.substr(0,pos1.get());
                    returnUrl.append(format("{}",m_dst));
                    returnUrl.append(url.substr(pos1.get()+15));
                    Terminal.print(0,'The wallet address was confirmed.\nTo finalize wallet authorization procedure you HAVE to use the link below.\n⚠️ Authorization will fail if you ignore the link!');
                    Terminal.print(0,returnUrl);
                }
                else {
                    printError();
                }
            }
            else {
                printError();
            }
        } else {
            printError();
        }
    }

    function getPayMsg(address sender, address recipient, uint128 amount, TvmCell payload) public pure
        returns(TvmCell message) {
        TvmCell body = tvm.encodeBody(SurfAuthDebot.pay, sender, recipient, amount, payload);
        TvmBuilder message_;
        message_.store(false, true, true, false, address(0), address(this));
        message_.storeTons(0);
        message_.storeUnsigned(0, 1);
        message_.storeTons(0);
        message_.storeTons(0);
        message_.store(uint64(0));
        message_.store(uint32(0));
        message_.storeUnsigned(0, 1); //init: nothing$0
        message_.storeUnsigned(1, 1); //body: right$1
        message_.store(body);
        message = message_.toCell();
    }

    function pay(address sender, address recipient, uint128 amount, TvmCell payload) public {
        m_dst = recipient;
        m_amount = amount;
        m_payload = payload;
        m_sender = sender;
        UserInfo.getSigningBox(tvm.functionId(getPayUserSign));
    }

    function getPayUserSign(uint32 handle) public {
        m_sign = handle;
        UserInfo.getAccount(tvm.functionId(getPayUserAddress));
    }

    function getPayUserAddress (address value) public {
        if (value == m_sender){
            ConfirmInput.get(tvm.functionId(signPay), "You are suggested to confirm a transaction. Would you like to continue?");
        } else {
            Terminal.print(0,"Error: wrong sender address!");
        }
    }

    function signPay (bool value) public {
        if (value){
            optional(uint256) none;
            m_sendMsg = tvm.buildExtMsg({
                dest: m_sender,
                callbackId: tvm.functionId(paySuccess),
                onErrorId: tvm.functionId(payError),
                time: 0,
                expire: 0,
                sign: true,
                pubkey: none,
                signBoxHandle: m_sign,
                call: {AMSig.sendTransaction, m_dst, m_amount, true, 1, m_payload}
            });
            confirmPay(true);
        } else {
            Terminal.print(0,"Terminated!");
        }
    }

    function confirmPay(bool value) public {
        if (value) {
            tvm.sendrawmsg(m_sendMsg,1);
        } else {
            Terminal.print(0, "Terminated!");
        }
    }

    function payError(uint32 sdkError, uint32 exitCode) public {
        ConfirmInput.get(tvm.functionId(confirmPay), format("Transaction failed. Sdk error = {}, Error code = {}\nDo you want to retry?", sdkError, exitCode));
    }

    function paySuccess() public {
        Terminal.print(0,"Payment done!");
    }

    /*
    *  Implementation of DeBot
    */
    function getDebotInfo() public functionID(0xDEB) override view returns(
        string name, string version, string publisher, string caption, string author,
        address support, string hello, string language, string dabi, bytes icon
    ) {
        name = "NiFi Club";
        version = "0.4.0";
        publisher = "";
        caption = "";
        author = "";
        support = address.makeAddrStd(0, 0x0);
        hello = "";
        language = "en";
        dabi = m_debotAbi.get();
        icon = m_icon;
    }

    function getRequiredInterfaces() public view override returns (uint256[] interfaces) {
        return [ Terminal.ID, Sdk.ID, Hex.ID, Base64.ID];
    }

    /*
    *  Implementation of Upgradable
    */
    function onCodeUpgrade() internal override {
        tvm.resetStorage();
    }

}
